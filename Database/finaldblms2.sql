-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: databaselms
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `databaselms`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `databaselms` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `databaselms`;

--
-- Table structure for table `designation_dl_map`
--

DROP TABLE IF EXISTS `designation_dl_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designation_dl_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) NOT NULL,
  `distribution_list` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_designation_dl_map_1` (`designation`),
  CONSTRAINT `fk_designation_dl_map_1` FOREIGN KEY (`designation`) REFERENCES `designation_leave_map` (`designation`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designation_dl_map`
--

LOCK TABLES `designation_dl_map` WRITE;
/*!40000 ALTER TABLE `designation_dl_map` DISABLE KEYS */;
INSERT INTO `designation_dl_map` VALUES (1,'tech_hr','ghanshyam@jasperindia.com'),(2,'engineering_manager','ghanshyam@jasperindia.com'),(3,'software_engineer','ghanshyam@jasperindia.com'),(4,'product_analyst','ghanshyam@jasperindia.com'),(5,'product_analyst_manager','ghanshyam@jasperindia.com'),(6,'product_hr','ghanshyam@jasperindia.com');
/*!40000 ALTER TABLE `designation_dl_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designation_leave_map`
--

DROP TABLE IF EXISTS `designation_leave_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designation_leave_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) NOT NULL,
  `casual_leave_allowed` int(11) NOT NULL,
  `sick_leave_allowed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `designation_UNIQUE` (`designation`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designation_leave_map`
--

LOCK TABLES `designation_leave_map` WRITE;
/*!40000 ALTER TABLE `designation_leave_map` DISABLE KEYS */;
INSERT INTO `designation_leave_map` VALUES (1,'tech_hr',18,8),(2,'engineering_manager',18,8),(3,'software_engineer',18,8),(4,'product_analyst',18,8),(5,'product_analyst_manager',18,8),(6,'product_hr',18,8);
/*!40000 ALTER TABLE `designation_leave_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_status_list`
--

DROP TABLE IF EXISTS `emp_status_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emp_status_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_status` varchar(50) NOT NULL,
  `status_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_status_UNIQUE` (`emp_status`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_status_list`
--

LOCK TABLES `emp_status_list` WRITE;
/*!40000 ALTER TABLE `emp_status_list` DISABLE KEYS */;
INSERT INTO `emp_status_list` VALUES (1,'on_role','Employee is on role, leave request can be made by employee'),(2,'on_leave','Employee is on leave, leave request can be made by employee'),(3,'on_probation','Employee is on probation, leave equest can\'t be made by employee'),(4,'serving_notice','Employee has given serving notice, leave request can\'t be made by employee'),(5,'resigned','Employee has resigned from firm, leave request can\'t be made by employee');
/*!40000 ALTER TABLE `emp_status_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dept` varchar(20) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `date_of_joining` date NOT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `designation` varchar(50) NOT NULL,
  `sick_leave_balance` int(11) NOT NULL,
  `casual_leave_balance` int(11) NOT NULL,
  `emp_status` varchar(50) NOT NULL,
  `hand_over_to_emp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_id_UNIQUE` (`emp_id`),
  KEY `fk_employee_1` (`designation`),
  KEY `fk_employee_2` (`emp_status`),
  KEY `fk_employee_3` (`manager_id`),
  KEY `fk_employee_4` (`hand_over_to_emp`),
  CONSTRAINT `fk_employee_1` FOREIGN KEY (`designation`) REFERENCES `designation_leave_map` (`designation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_2` FOREIGN KEY (`emp_status`) REFERENCES `emp_status_list` (`emp_status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_3` FOREIGN KEY (`manager_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_employee_4` FOREIGN KEY (`hand_over_to_emp`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,1,'abc','Technology','madhu.kiran@jasperindia.com','2012-01-01',1,'engineering_manager',2,4,'on_role',2),(2,2,'zxcv','Technology','madhu.kiran@jasperindia.com','2012-01-01',2,'engineering_manager',2,4,'on_role',1),(3,3,'qwer','Technology','madhu.kiran@jasperindia.com','2012-01-01',1,'software_engineer',2,4,'on_role',1),(4,4,'asdf','Technology','madhu.kiran@jasperindia.com','2012-01-01',1,'software_engineer',2,4,'on_role',1),(5,5,'ghjk','Technology','madhu.kiran@jasperindia.com','2012-01-01',2,'software_engineer',2,4,'on_role',2),(6,6,'pqrs','Technology','madhu.kiran@jasperindia.com','2012-01-01',6,'tech_hr',2,4,'on_role',7),(7,7,'pqrs2','Technology','madhu.kiran@jasperindia.com','2012-01-01',6,'tech_hr',2,4,'on_role',6),(8,8,'pro_1','Product','madhu.kiran@jasperindia.com','2012-01-01',8,'product_analyst_manager',2,4,'on_role',9),(9,9,'pro_2','Product','madhu.kiran@jasperindia.com','2012-01-01',9,'product_analyst_manager',2,4,'on_role',8),(10,10,'pro_3','Product','madhu.kiran@jasperindia.com','2012-01-01',8,'product_analyst',2,4,'on_role',8),(11,11,'pro_4','Product','madhu.kiran@jasperindia.com','2012-01-01',8,'product_analyst',2,4,'on_role',8),(12,12,'pro_5','Product','madhu.kiran@jasperindia.com','2012-01-01',9,'product_analyst',2,4,'on_role',9),(13,13,'pro_6','Product','madhu.kiran@jasperindia.com','2012-01-01',13,'product_hr',2,4,'on_role',14),(14,14,'pro_7','Product','madhu.kiran@jasperindia.com','2012-01-01',13,'product_hr',2,4,'on_role',13);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_request`
--

DROP TABLE IF EXISTS `leave_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_request` (
  `leave_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `leave_from` date NOT NULL,
  `leave_to` date NOT NULL,
  `applied_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `state` varchar(50) NOT NULL DEFAULT 'PENDING_APPROVAL',
  `leave_type` varchar(50) NOT NULL DEFAULT 'CASUAL',
  `leave_reason` text NOT NULL,
  `no_of_leave_days` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`leave_id`),
  KEY `fk_leave_request_1` (`emp_id`),
  KEY `fk_leave_request_2` (`state`),
  KEY `fk_leave_request_3` (`leave_type`),
  CONSTRAINT `fk_leave_request_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_request_2` FOREIGN KEY (`state`) REFERENCES `leave_state_list` (`state`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_request_3` FOREIGN KEY (`leave_type`) REFERENCES `leave_type_list` (`leave_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_request`
--

LOCK TABLES `leave_request` WRITE;
/*!40000 ALTER TABLE `leave_request` DISABLE KEYS */;
INSERT INTO `leave_request` VALUES (1,3,'2012-09-19','2012-09-19','2012-09-06 18:30:00','pending_approval_by_manager','casual','Going Home',1),(2,4,'2012-09-28','2012-10-03','2012-09-06 18:30:00','pending_approval_by_manager','casual','Alumni Meet',3);
/*!40000 ALTER TABLE `leave_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_state_list`
--

DROP TABLE IF EXISTS `leave_state_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_state_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(50) NOT NULL,
  `state_desc` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state_UNIQUE` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_state_list`
--

LOCK TABLES `leave_state_list` WRITE;
/*!40000 ALTER TABLE `leave_state_list` DISABLE KEYS */;
INSERT INTO `leave_state_list` VALUES (1,'null','leave request not created'),(2,'pending_approval_by_manager','Leave request has been submitted by employee with pending approval from his/her manger and notification to self and manger'),(3,'approved_by_manager','Leave request approved by manager, notification to self, manager and corresponding HR/group'),(4,'rejected_by_manager','Leave request rejected by manager, notification to self and manager'),(5,'expired_by_date','Leave request not approved/rejected by manager, notification to self and manager'),(6,'deleted_by_employee','Leave request deleted by employee, notification to self, manger and HR/Group(depends on previous leave state)');
/*!40000 ALTER TABLE `leave_state_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_state_log`
--

DROP TABLE IF EXISTS `leave_state_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_state_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `leave_msg` text NOT NULL,
  `prev_state` varchar(50) DEFAULT NULL,
  `curr_state` varchar(50) NOT NULL,
  `time_of_change` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logger_emp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_leave_state_log_1` (`leave_id`),
  KEY `fk_leave_state_log_2` (`emp_id`),
  KEY `fk_leave_state_log_3` (`prev_state`),
  KEY `fk_leave_state_log_4` (`curr_state`),
  KEY `fk_leave_state_log_5` (`logger_emp_id`),
  CONSTRAINT `fk_leave_state_log_1` FOREIGN KEY (`leave_id`) REFERENCES `leave_request` (`leave_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_state_log_2` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_state_log_3` FOREIGN KEY (`prev_state`) REFERENCES `leave_state_list` (`state`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_state_log_4` FOREIGN KEY (`curr_state`) REFERENCES `leave_state_list` (`state`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_leave_state_log_5` FOREIGN KEY (`logger_emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_state_log`
--

LOCK TABLES `leave_state_log` WRITE;
/*!40000 ALTER TABLE `leave_state_log` DISABLE KEYS */;
INSERT INTO `leave_state_log` VALUES (1,1,3,'Going Home','null','pending_approval_by_manager','2012-09-07 10:16:37',3),(2,2,4,'Alumni Meet','null','pending_approval_by_manager','2012-09-07 10:17:05',4);
/*!40000 ALTER TABLE `leave_state_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leave_type_list`
--

DROP TABLE IF EXISTS `leave_type_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leave_type_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_type` varchar(50) NOT NULL,
  `leave_type_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `leave_type_UNIQUE` (`leave_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leave_type_list`
--

LOCK TABLES `leave_type_list` WRITE;
/*!40000 ALTER TABLE `leave_type_list` DISABLE KEYS */;
INSERT INTO `leave_type_list` VALUES (1,'sick','sick leave'),(2,'casual','casual leave'),(3,'maternity','maternity leave');
/*!40000 ALTER TABLE `leave_type_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `official_holidays`
--

DROP TABLE IF EXISTS `official_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `official_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `holiday` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `official_holidays`
--

LOCK TABLES `official_holidays` WRITE;
/*!40000 ALTER TABLE `official_holidays` DISABLE KEYS */;
INSERT INTO `official_holidays` VALUES (1,'2012-10-02','Gandhi Jayanti'),(2,'2012-10-24','Dussehra'),(3,'2012-11-13','Diwali'),(4,'2012-12-25','Christmas'),(5,'2013-01-26','Republic Day');
/*!40000 ALTER TABLE `official_holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwd_role`
--

DROP TABLE IF EXISTS `passwd_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwd_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_passwd_role_1` (`emp_id`),
  CONSTRAINT `fk_passwd_role_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwd_role`
--

LOCK TABLES `passwd_role` WRITE;
/*!40000 ALTER TABLE `passwd_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwd_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_emp_map`
--

DROP TABLE IF EXISTS `role_emp_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_emp_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_emp_map_1` (`emp_id`),
  KEY `fk_role_emp_map_2` (`roles`),
  CONSTRAINT `fk_role_emp_map_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_emp_map_2` FOREIGN KEY (`roles`) REFERENCES `role_master_list` (`roles`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_emp_map`
--

LOCK TABLES `role_emp_map` WRITE;
/*!40000 ALTER TABLE `role_emp_map` DISABLE KEYS */;
INSERT INTO `role_emp_map` VALUES (1,'Employee',1),(2,'Manager',1),(3,'Employee',2),(4,'Manager',2),(5,'Employee',3),(6,'Employee',4),(7,'Employee',5),(8,'Employee',6),(9,'Manager',6),(10,'HR',6),(11,'Employee',7),(12,'HR',7),(37,'Employee',8),(38,'Manager',8),(39,'Employee',9),(40,'Manager',9),(41,'Employee',10),(42,'Employee',11),(43,'Employee',12),(44,'Employee',13),(45,'Manager',13),(46,'HR',13),(47,'Employee',14),(48,'HR',14);
/*!40000 ALTER TABLE `role_emp_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_master_list`
--

DROP TABLE IF EXISTS `role_master_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_master_list` (
  `roles` varchar(50) NOT NULL,
  `role_desc` varchar(100) NOT NULL,
  PRIMARY KEY (`roles`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_master_list`
--

LOCK TABLES `role_master_list` WRITE;
/*!40000 ALTER TABLE `role_master_list` DISABLE KEYS */;
INSERT INTO `role_master_list` VALUES ('Employee','Employee'),('HR','Human Resource Department Employee'),('Manager','Manager');
/*!40000 ALTER TABLE `role_master_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_email_map`
--

DROP TABLE IF EXISTS `state_email_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_email_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) NOT NULL,
  `prev_state` varchar(50) NOT NULL,
  `curr_state` varchar(50) NOT NULL,
  `email_group` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_state_email_map_1` (`designation`),
  KEY `fk_state_email_map_2` (`prev_state`),
  KEY `fk_state_email_map_3` (`curr_state`),
  CONSTRAINT `fk_state_email_map_1` FOREIGN KEY (`designation`) REFERENCES `designation_leave_map` (`designation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_state_email_map_2` FOREIGN KEY (`prev_state`) REFERENCES `leave_state_list` (`state`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_state_email_map_3` FOREIGN KEY (`curr_state`) REFERENCES `leave_state_list` (`state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_email_map`
--

LOCK TABLES `state_email_map` WRITE;
/*!40000 ALTER TABLE `state_email_map` DISABLE KEYS */;
INSERT INTO `state_email_map` VALUES (1,'software_engineer','null','pending_approval_by_manager','SM'),(2,'software_engineer','pending_approval_by_manager','approved_by_manager','SMH'),(3,'software_engineer','pending_approval_by_manager','rejected_by_manager','SM'),(4,'software_engineer','pending_approval_by_manager','expired_by_date','SM'),(5,'software_engineer','pending_approval_by_manager','deleted_by_employee','SM'),(6,'software_engineer','approved_by_manager','deleted_by_employee','SMH'),(7,'tech_hr','null','pending_approval_by_manager','SM'),(8,'tech_hr','pending_approval_by_manager','approved_by_manager','SMH'),(9,'tech_hr','pending_approval_by_manager','rejected_by_manager','SM'),(10,'tech_hr','pending_approval_by_manager','expired_by_date','SM'),(11,'tech_hr','pending_approval_by_manager','deleted_by_employee','SM'),(12,'tech_hr','approved_by_manager','deleted_by_employee','SMH'),(13,'engineering_manager','null','pending_approval_by_manager','SM'),(14,'engineering_manager','pending_approval_by_manager','approved_by_manager','SMH'),(15,'engineering_manager','pending_approval_by_manager','rejected_by_manager','SM'),(16,'engineering_manager','pending_approval_by_manager','expired_by_date','SM'),(17,'engineering_manager','pending_approval_by_manager','deleted_by_employee','SM'),(18,'engineering_manager','approved_by_manager','deleted_by_employee','SMH'),(19,'product_analyst','null','pending_approval_by_manager','SM'),(20,'product_analyst','pending_approval_by_manager','approved_by_manager','SMH'),(21,'product_analyst','pending_approval_by_manager','rejected_by_manager','SM'),(22,'product_analyst','pending_approval_by_manager','expired_by_date','SM'),(23,'product_analyst','pending_approval_by_manager','deleted_by_employee','SM'),(24,'product_analyst','approved_by_manager','deleted_by_employee','SMH'),(25,'product_analyst_manager','null','pending_approval_by_manager','SM'),(26,'product_analyst_manager','pending_approval_by_manager','approved_by_manager','SMH'),(27,'product_analyst_manager','pending_approval_by_manager','rejected_by_manager','SM'),(28,'product_analyst_manager','pending_approval_by_manager','expired_by_date','SM'),(29,'product_analyst_manager','pending_approval_by_manager','deleted_by_employee','SM'),(30,'product_analyst_manager','approved_by_manager','deleted_by_employee','SMH'),(31,'product_hr','null','pending_approval_by_manager','SM'),(32,'product_hr','pending_approval_by_manager','approved_by_manager','SMH'),(33,'product_hr','pending_approval_by_manager','rejected_by_manager','SM'),(34,'product_hr','pending_approval_by_manager','expired_by_date','SM'),(35,'product_hr','pending_approval_by_manager','deleted_by_employee','SM'),(36,'product_hr','approved_by_manager','deleted_by_employee','SMH');
/*!40000 ALTER TABLE `state_email_map` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-11 10:46:05
