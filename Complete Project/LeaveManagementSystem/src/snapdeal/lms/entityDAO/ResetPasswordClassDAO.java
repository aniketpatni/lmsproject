package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * @author arif
 * 
 */
public class ResetPasswordClassDAO {

	public static int addResetPasswordClass(String extension) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		

		try {
			transaction = session.beginTransaction();
			ResetPasswordClass respass = new ResetPasswordClass();

			respass.setExtension(extension);
			 session.save(respass);
			transaction.commit();
			return 1;
		} catch (HibernateException e) {
			transaction.rollback();
			return 0;
		
		} finally {
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	public String listResetPasswordById(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<ResetPasswordClass> passwordList = session.createQuery(
					"from ResetPasswordClass where id=" + id).list();
			transaction.commit();
			return passwordList.get(0).getExtension();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<ResetPasswordClass> listResetPasswordClass() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<ResetPasswordClass> resetPasswordClasses = session.createQuery(
					"from ResetPasswordClass").list();

			/*for (RoleMasterList roleMasterList : roleMasterLists) {
				System.out.println(roleMasterList.toString());
			}*/

			transaction.commit();
			return resetPasswordClasses;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	public void updateResetPasswordClass(int extensionId, String updatedExtension) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			ResetPasswordClass respass = (ResetPasswordClass) session.get(
					ResetPasswordClass.class, extensionId);
			respass.setExtension(updatedExtension);
			session.save(respass);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			
		} finally {
			session.close();
		}
	}

	public void deleteResetPasswordClass(int extensionId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			ResetPasswordClass resPass = (ResetPasswordClass) session.get(ResetPasswordClass.class, extensionId);
			session.delete(resPass);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
