package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class StateEmailMapDAO {

	public static Long saveStateEmailMap(String prev_state, String curr_state, String designation, String email_group) throws NullPointerException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long mapId = null;
		try {
			transaction = session.beginTransaction();
			StateEmailMap stateEmailMapObject = new StateEmailMap();
			stateEmailMapObject.setPrevState(new LeaveStateList(prev_state));
			stateEmailMapObject.setCurrState(new LeaveStateList(curr_state));
			stateEmailMapObject.setDesignation(new DesignationLeaveMapDAO().getObjectByDesignation(designation));
			stateEmailMapObject.setEmailGroup(email_group);
			session.save(stateEmailMapObject);
			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return mapId;
	}

	@SuppressWarnings("unchecked")
	public static StateEmailMap getStateEmailMap(String designation, String prevState, String currState) {
		// state_email_map state_email_mapObject = new state_email_map();
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			
			List<StateEmailMap> emailStates = session.createQuery("from StateEmailMap where prevState.state='"+prevState+"' and currState.state='"+currState+"' and designation.designation='"+designation+"'").list();
			
			transaction.commit();
			return emailStates.get(0);
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static List<StateEmailMap> getStateEmailMap() {
		// state_email_map state_email_mapObject = new state_email_map();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<StateEmailMap> emailStates = session.createQuery("from StateEmailMap").list();
			transaction.commit();
			return emailStates;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static void updateEmailGroup(String designation, String prev_state, String curr_state, String email_group) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<StateEmailMap> state_email_mapObject = session.createQuery("from StateEmailMap where designation.designation='"+designation+"' and prevState.state='"+prev_state+"' and currState.state='"+curr_state+"'").list();
			state_email_mapObject.get(0).setEmailGroup(email_group);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static int deleteStateEmailMap(String designation, String prev_state, String curr_state) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<StateEmailMap> state_email_mapObject = session.createQuery("from StateEmailMap where designation.designation='"+designation+"' and prevState.state='"+prev_state+"' and currState.state='"+curr_state+"'").list();
			session.delete(state_email_mapObject.get(0));
			transaction.commit();
			return 1;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return 0;
	}

}
