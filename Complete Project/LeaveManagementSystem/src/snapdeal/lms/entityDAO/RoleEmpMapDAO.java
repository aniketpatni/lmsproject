package snapdeal.lms.entityDAO;



import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class RoleEmpMapDAO {

	@SuppressWarnings("unchecked")
	public static String saveRoles(int empId,String roles)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<RoleEmpMap> remList = session.createQuery("from RoleEmpMap where emp.empId="+empId).list();
			if(remList.size()!=0){
				int flag=0;
				for(RoleEmpMap rem : remList){
					if(rem.getRole().getRoles().equals(roles)){
						flag=1;
					}
				}
				if(flag==1){
					transaction.commit();
					return "Duplication avoided";
				}
			}
			RoleEmpMap roll = new RoleEmpMap();
			roll.setEmp(EmployeeDAO.getInstance().getEmployee(empId));
			roll.setRole(RoleMasterListDAO.listObjectByRole(roles));
			session.save(roll);
			transaction.commit();
			return "Role saved";
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static List<RoleEmpMap> listRolesByEmpId(int empId)
	{
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<RoleEmpMap> roleEmpMapList = session.createQuery("from RoleEmpMap where emp.empId="+empId).list();
			transaction.commit();
			return roleEmpMapList;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	
	public static void updateRoles(int empId,String roles)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			RoleEmpMap role = (RoleEmpMap) session.get(RoleEmpMap.class, empId);
			role.setRole(RoleMasterListDAO.listObjectByRole(roles));
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static void deleteRoles(int empId)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<RoleEmpMap> roleEmpMapList = session.createQuery("from RoleEmpMap where emp.empId="+empId).list();
			for(RoleEmpMap rem : roleEmpMapList){
				session.delete(rem);
			}
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
}