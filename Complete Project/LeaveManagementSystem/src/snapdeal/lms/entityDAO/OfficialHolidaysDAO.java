package snapdeal.lms.entityDAO;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class OfficialHolidaysDAO {

	public Long saveHoliday(String holidayName, Date date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long holidayId = null;
		try {
			transaction = session.beginTransaction();
			OfficialHolidays holiday = new OfficialHolidays();
			holiday.setHoliday(holidayName);
			holiday.setDate(date);
			holidayId = (Long) session.save(holiday);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return holidayId;
	}

	@SuppressWarnings("unchecked")
	public List<OfficialHolidays> listHolidays() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<OfficialHolidays> holidays = null;
		try {
			transaction = session.beginTransaction();
			holidays = session.createQuery("from OfficialHolidays").list();
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return holidays;
	}

	@SuppressWarnings("unchecked")
	public List<OfficialHolidays> listHolidaysAsc() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<OfficialHolidays> holidays = null;
		try {
			transaction = session.beginTransaction();
			holidays = session.createQuery("from OfficialHolidays order by date asc").list();
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return holidays;
	}

	
	public void updateHolidays(Long holidayId, String holidayName, Date date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			OfficialHolidays holiday = (OfficialHolidays) session.get(OfficialHolidays.class, holidayId);
			holiday.setHoliday(holidayName);
			holiday.setDate(date);
			transaction.commit();
			
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void deleteHolidays(String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			OfficialHolidays holiday = (OfficialHolidays) session.get(OfficialHolidays.class, name);
			session.delete(holiday);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void deleteHolidaysByDate(String date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			OfficialHolidays oh1 = (OfficialHolidays) session.createQuery("from OfficialHolidays where date='"+date+"'").uniqueResult();
			session.delete(oh1);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public int listNoOfHolidaysBetweenDates(Date startDate, Date endDate) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<OfficialHolidays> holidays = null;
		try {
			transaction = session.beginTransaction();
			holidays = session.createQuery("from OfficialHolidays where date>'"+startDate+"' and date<'"+endDate+"'").list();
			transaction.commit();
			return holidays.size();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return 0;
	}
}