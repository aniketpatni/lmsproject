package snapdeal.lms.entityDAO;




import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="reset_password")
public class ResetPasswordClass{


		private int id;
		private String extension;
		
		public ResetPasswordClass() {}

		@Id
		@GeneratedValue
		@Column(name="id")
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

     	@Column(name="extension", nullable=false)
//		@OneToOne(fetch=FetchType.EAGER)
//		@OneToOne
//		@JoinColumn(name="emp_id", referencedColumnName="emp_id")
		public String getExtension(){
			return extension;
		}
		
		public void setExtension(String extension){
			this.extension=extension;
		}

		
}
