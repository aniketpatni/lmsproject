package snapdeal.lms.entityDAO;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmpStatusListDAO {
	
	public int addEmpStatus(String empStatus, String statusDesc){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try{	
			transaction = session.beginTransaction();
			EmpStatusList emplStatus=new EmpStatusList();
			emplStatus.setEmpStatus(empStatus);
			emplStatus.setStatusDesc(statusDesc);
			session.save(emplStatus);
			transaction.commit();
			return 1;
		}
		catch(HibernateException e){
			transaction.rollback();
			return 0;
		}
		finally{
			session.close();
		}
			
	}
	
	@SuppressWarnings("unchecked")
	public int updateEmpStatus(String currEmpStatus, String newStatusDesc) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try{			
			transaction = session.beginTransaction();
			List<EmpStatusList> empStatusList= session.createQuery("from EmpStatusList where empStatus='"+currEmpStatus+"'").list();
			EmpStatusList emplStatus=empStatusList.get(0);
			emplStatus.setStatusDesc(newStatusDesc);
			session.save(emplStatus);
			transaction.commit();
			return 1;
		}
		catch(HibernateException e){
			transaction.rollback();
			return 0;
		}
		finally{
			session.close();
		}
	}
	
	public List<EmpStatusList> getEmpStatusList(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<EmpStatusList> empStatusList = session.createQuery("from EmpStatusList").list();			
			transaction.commit();
			return empStatusList;
		}
		catch(HibernateException e){
			transaction.rollback();
			return null;
		}
		finally{
			session.close();
		}
	}
	
	public static EmpStatusList getEmpStatusByStatus(String empStatus) throws NullPointerException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<EmpStatusList> empStatusList = session.createQuery("from EmpStatusList where empStatus='"+empStatus+"'").list();			
			transaction.commit();
			return empStatusList.get(0);
		}
		catch(HibernateException e){
			transaction.rollback();
			return null;
		}
		finally{
			session.close();
		}
	}
	
	public int deleteEmpStatus(String empStatus){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try{	
			transaction = session.beginTransaction();
			String query="from EmpStatusList where empStatus='"+empStatus+"'";
			EmpStatusList empStatusList=(EmpStatusList)session.createQuery(query).list().get(0);
			session.delete(empStatusList);
			transaction.commit();
			return 1;
		}
		catch(HibernateException e){
			transaction.rollback();
			return 0;
		}
		finally{
			session.close();
		}
	}
}