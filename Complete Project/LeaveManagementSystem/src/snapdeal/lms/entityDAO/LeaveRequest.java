package snapdeal.lms.entityDAO;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "leave_request")
public class LeaveRequest implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6517788506277453156L;
	private int leaveId;
	private Employee emp;
	private Date leaveFrom;
	private Date leaveTo;
	private Timestamp appliedOn;
	private LeaveStateList state;
	private LeaveType leaveType;
	private String leaveReason;
	private int noOfLeaveDays;

	/**
	 * 
	 * @return
	 */
	@Id
	@GeneratedValue
	@Column(name = "leave_id")
	public int getLeaveId() {
		return leaveId;
	}

	/**
	 * 
	 * @param leaveId
	 */
	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "emp_id",referencedColumnName="emp_id")
	public Employee getEmp() {
		return emp;
	}

	/**
	 * 
	 * @param empId
	 */
	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "leave_from", nullable = false)
	public Date getLeaveFrom() {
		return leaveFrom;
	}

	/**
	 * 
	 * @param leaveFrom
	 */
	public void setLeaveFrom(Date leaveFrom) {
		this.leaveFrom = leaveFrom;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "leave_to", nullable = false)
	public Date getLeaveTo() {
		return leaveTo;
	}

	/**
	 * 
	 * @param leaveTo
	 */
	public void setLeaveTo(Date leaveTo) {
		this.leaveTo = leaveTo;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "applied_on", nullable = true)
	public Timestamp getAppliedOn() {
		return appliedOn;
	}

	/**
	 * 
	 * @param appliedOn
	 */
	public void setAppliedOn(Timestamp appliedOn) {
		this.appliedOn = appliedOn;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "state", referencedColumnName="state")
	public LeaveStateList getState() {
		return state;
	}

	/**
	 * 
	 * @param state
	 */
	public void setState(LeaveStateList state) {
		this.state = state;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "leave_type", referencedColumnName="leave_type")
	public LeaveType getLeaveType() {
		return leaveType;
	}

	/**
	 * 
	 * @param leaveType
	 */
	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "leave_reason", nullable = false)
	public String getLeaveReason() {
		return leaveReason;
	}

	/**
	 * 
	 * @param leaveReason
	 */
	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "no_of_leave_days", nullable = false)
	public int getNoOfLeaveDays() {
		return noOfLeaveDays;
	}

	/**
	 * 
	 * @param noOfLeaveDays
	 */
	public void setNoOfLeaveDays(int noOfLeaveDays) {
		this.noOfLeaveDays = noOfLeaveDays;
	}

	/**
 * 
 */
	@Override
	public String toString() {
		return "Leave [leaveId=" + leaveId + ", emp=" + emp
				+ ", leaveFrom=" + leaveFrom + ", leaveTo=" + leaveTo
				+ ", appliedOn=" + appliedOn + ", state=" + state
				+ ", leaveType=" + leaveType + ", leaveReason=" + leaveReason
				+ ", noOfLeaveDays=" + noOfLeaveDays + "]";
	}

}