package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role_master_list")
public class RoleMasterList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2274578141913658229L;
	private String roles;
	private String roledesc;

	@Id
	@Column(name = "roles", unique=true)
	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@Column(name = "role_desc")
	public String getRoleDesc() {
		return roledesc;
	}

	public void setRoleDesc(String roledesc) {
		this.roledesc = roledesc;
	}

}