package snapdeal.lms.util;

import java.util.HashMap;

public class SubjectofMail {

	public String nulltobeApprovedtoManager = "Request of Leave from EMPLOYEENAME";
	public String nulltobeApprovedtoEmployee = "Request submitted with leaveid: LEAVEID";

	public String approvedbyManagertoManager = "Leave confirmation of EMPLOYEENAME ";
	public String approvedbyManagertoEmployee = "Leave Approved";
	public String approvedbyManagertoHR = "Leave Approval Notification of EMPLOYEENAME";

	public String rejectedbyManagertoManager = "Leave rejection of EMPLOYEENAME ";
	public String rejecetdbyManagertoEmployee = "Leave request rejected";

	public String expiredtoManager = "Expiry of leave request of EMPLOYEENAME";
	public String expiredtoEmployee = "Leave request expired";

	public String tobeApprovedDeletedtoManager = "Deletion of Leave request of EMPLOYEENAME";
	public String tobeApprovedDeletedtoEmployee = "Leave request is successfully cancelled";

	public String approvedDeletedtoManager = "Approved Leave is cancelled by EMPLOYEENAME";
	public String approvedDeletedtoEmployee = "Approved leave is cancellled successfully";
	public String approvedDeletedtoHR = "Appoved Leave is cancelled by EMPLOYEENAME";
	
	public String updatedtoEmployee = "Leave Request Updated";
	public String updatedtoManager = "Updated Leave Request by EMPLOYEENAME";
	
	public String passwordmailsubject="New Password set";
	
	@SuppressWarnings("rawtypes")
	public String getSubject(String prevState, String currentState, String SMH,
			HashMap mailDetails) {

		/*
		 * case 1st | null| pending_approval_by_manager
		 */
		if (prevState.contains("null")
				&& currentState.contains("pending_approval_by_manager")) {
			if (SMH.equals("S")) {
				return (nulltobeApprovedtoEmployee.replace("LEAVEID",
						(CharSequence) ((Integer) mailDetails.get("leaveId"))
								.toString()));
			} else if (SMH.equals("M")) {
				return (nulltobeApprovedtoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName")));
			}
		}
		/*
		 * case 2nd | pending_approval_by_manager | approved_by_manager
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("approved_by_manager")) {
			if (SMH.equals("S")) {
				return approvedbyManagertoEmployee;
			} else if (SMH.equals("M")) {
				return approvedbyManagertoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			} else if (SMH.equals("H")) {
				return approvedbyManagertoHR.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			}
		}

		/*
		 * case 3rd | pending_approval_by_manager | rejected_by_manager
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("rejected_by_manager")) {
			if (SMH.equals("S")) {
				return rejecetdbyManagertoEmployee;
			} else if (SMH.equals("M")) {
				return rejectedbyManagertoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			}
		}

		/*
		 * case 4 | pending_approval_by_manager | expired_by_date
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("expired_by_date")) {
			if (SMH.equals("S")) {
				return expiredtoEmployee;
			} else if (SMH.equals("M")) {
				return expiredtoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			}
		}

		/*
		 * case 5 | pending_approval_by_manager | deleted_by_employee
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("deleted_by_employee")) {
			if (SMH.equals("S")) {
				return tobeApprovedDeletedtoEmployee;
			} else if (SMH.equals("M")) {
				return tobeApprovedDeletedtoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			}
		}

		/*
		 * case 6 | approved_by_manager | deleted_by_employee
		 */

		if (prevState.contains("approved_by_manager")
				&& currentState.contains("deleted_by_employee")) {
			if (SMH.equals("S")) {
				return approvedDeletedtoEmployee;
			} else if (SMH.equals("M")) {
				return approvedDeletedtoManager.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			} else if (SMH.equals("H")) {
				return approvedDeletedtoHR.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
			}
		}
		
		/*
		 * case 7
		 */
		if (prevState.contains("deleted_by_employee")
				&& currentState.contains("pending_approval_by_manager")) {
			if (SMH.equals("S")) {
				return updatedtoEmployee;
			} else if (SMH.equals("M")) {
				return updatedtoManager.replace("EMPLOYEENAME", (CharSequence) mailDetails.get("empName"));
			}
		}

		return null;
	}
}
