package snapdeal.lms.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;

public class LeaveExpiryJobs implements Job{

	public LeaveExpiryJobs() {
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date currDate = new Date();
		String currDateStr = sdf.format(cal.getTime());
		System.out.println(currDateStr);
		List<LeaveRequest> lrList = LeaveRequestDAO.getInstance().getLeaveRequestsBeforeDate(currDateStr);
		for(LeaveRequest lr1 : lrList){
			Date leaveFrom = lr1.getLeaveFrom();
			if(!leaveFrom.after(currDate)&&lr1.getState().getState().contains("pending_approval")){
				LeaveRequestDAO.getInstance().updateLeaveRequestState(lr1.getLeaveId(), "expired_by_date", "This leave request has expired ! :Generated By Scheduled Job:", lr1.getEmp().getManager().getEmpId());
			}
		}
		
	}

}
