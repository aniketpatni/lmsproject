package snapdeal.lms.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import snapdeal.lms.entityDAO.*;

public class SendMail {

	private static volatile SendMail onlyInstance = null;

	public static SendMail getInstance() {
		if (onlyInstance == null) {
			synchronized (LeaveStateLogDAO.class) {
				if (onlyInstance == null) {
					onlyInstance = new SendMail();
				}
			}
		}
		return onlyInstance;
	}

	public void send(String empIdTemp, String link, String code) {
		int empId = Integer.parseInt(empIdTemp);
		EmployeeDAO empDao = EmployeeDAO.getInstance();
		Employee emp = empDao.getEmployee(empId);
		String emialId = emp.getEmailId();
		String mailsubject = "New Password";
		String messagecontent = (new MessageOfMail().getMessageContent(link,
				emp.getName(), code));
		sendUsingGmail(emialId, (new AdminDetail().getAdminEmailID()),
				messagecontent, mailsubject);
	}

	@SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
	public void send(int leaveId) {

		LeaveStateLogDAO leaveLogDao = LeaveStateLogDAO.getInstance();
		// LeaveRequest leaveRequest =
		// LeaveRequestDAO.getInstance().getLeaveRequestByLeaveId(leaveId).get(0);
		List<LeaveStateLog> leaveStateloglist = leaveLogDao.getHistory(leaveId);
		LeaveStateLog leave = leaveStateloglist
				.get(leaveStateloglist.size() - 1);

		int empId = leave.getEmp().getId();
		String empName = leave.getEmp().getName();
		String empEmailId = leave.getEmp().getEmailId();
		String managerName = leave.getEmp().getManager().getName();
		String managerEmailId = leave.getEmp().getManager().getEmailId();
		String curState = leave.getCurrState().getState();
		String prevState = leave.getPrevState().getState();
		String hrMailGroup = new DesignationDLMapDAO().getDLByDesignation(leave
				.getEmp().getDesignation().getDesignation());

		Date leaveFrom = leave.getLeave().getLeaveFrom();
		Date leaveTo = leave.getLeave().getLeaveTo();
		long duration = (leaveTo.getTime() - leaveFrom.getTime())
				/ (60 * 60 * 1000 * 24);
		Timestamp appliedOn = leave.getLeave().getAppliedOn();
		String leaveReason = leave.getLeave().getLeaveReason();
		String remarks = leave.getLeaveMsg();
		int casualLeaveBalance = leave.getEmp().getCasualLeaveBalance();
		int sickLeaveBalance = leave.getEmp().getSickLeaveBalance();

		// String SMHPattern = new
		// StateEmailMapDAO().getStateEmailMap(leave.getEmp().getDesignation().getDesignation(),
		// prevState, curState).getEmailGroup();

		String s = leave.getEmp().getDesignation().getDesignation();
		StateEmailMap SMHPatter = new StateEmailMapDAO().getStateEmailMap(s,
				prevState, curState);
		String SMHPattern = SMHPatter.getEmailGroup();

		HashMap mailDetails = new HashMap();
		mailDetails.put("leaveId", leaveId);
		mailDetails.put("empId", empId);
		mailDetails.put("empName", empName);
		mailDetails.put("empEmailId", empEmailId);
		mailDetails.put("managerEmailId", managerEmailId);
		mailDetails.put("managerName", managerName);
		mailDetails.put("curState", curState);
		mailDetails.put("prevState", prevState);
		mailDetails.put("appliedOn", appliedOn);
		mailDetails.put("leaveFrom", leaveFrom);
		mailDetails.put("leaveTo", leaveTo);
		mailDetails.put("duration", duration);
		mailDetails.put("leaveReason", leaveReason);
		mailDetails.put("remarks", remarks);
		mailDetails.put("hrMailGroup", hrMailGroup);
		mailDetails.put("casualLeaveBalance", casualLeaveBalance);
		mailDetails.put("sickLeaveBalance", sickLeaveBalance);
		mailDetails.put("SMHPattern", SMHPattern);

		if (SMHPattern.contains("S")) {
			String subjectofmail = new SubjectofMail().getSubject(prevState,
					curState, "S", mailDetails);
			String messageofmail = new MessageOfMail().getMessageContent(
					prevState, curState, "S", mailDetails);
			sendUsingGmail(empEmailId, (new AdminDetail().getAdminEmailID()),
					messageofmail, subjectofmail);
		}
		if (SMHPattern.contains("M")) {
			String subjectofmail = new SubjectofMail().getSubject(prevState,
					curState, "M", mailDetails);
			String messageofmail = new MessageOfMail().getMessageContent(
					prevState, curState, "M", mailDetails);
			sendUsingGmail(empEmailId, (new AdminDetail().getAdminEmailID()),
					messageofmail, subjectofmail);
		}
		if (SMHPattern.contains("H")) {
			String subjectofmail = new SubjectofMail().getSubject(prevState,
					curState, "H", mailDetails);
			String messageofmail = new MessageOfMail().getMessageContent(
					prevState, curState, "H", mailDetails);
			sendUsingGmail(empEmailId, (new AdminDetail().getAdminEmailID()),
					messageofmail, subjectofmail);
		}
	}

	public void sendUsingGmail(String recipients, String fromaddress,
			String mailContent, String mailSubject) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication((new AdminDetail()
								.getAdminEmailID()), (new AdminDetail()
								.getAdminPassword()));
					}
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromaddress));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					recipients));

			Multipart multipart = new MimeMultipart();
			message.setSubject(mailSubject);

			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(mailContent, "text/html");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}