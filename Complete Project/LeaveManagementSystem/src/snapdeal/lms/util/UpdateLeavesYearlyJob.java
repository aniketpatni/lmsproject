package snapdeal.lms.util;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;

public class UpdateLeavesYearlyJob implements Job{

	private int carryOverLeavesCasual = 0;
	private int carryOverLeavesSick = 0;
	
	//Public no-arg constructor for Quartz
	public UpdateLeavesYearlyJob() {
			
	}
	
	public int getCarryOverLeaves() {
		return carryOverLeavesCasual;
	}

	public void setCarryOverLeaves(int carryOverLeaves) {
		this.carryOverLeavesCasual = carryOverLeaves;
	}

	public int getCarryOverLeavesSick() {
		return carryOverLeavesSick;
	}

	public void setCarryOverLeavesSick(int carryOverLeavesSick) {
		this.carryOverLeavesSick = carryOverLeavesSick;
	}

	public int removeFromCarryOverCasual(int currLeaves){
		if(currLeaves>carryOverLeavesCasual){
			return currLeaves-carryOverLeavesCasual;
		}else{
			return 0;
		}
	}
	
	public int removeFromCarryOverSick(int currLeaves){
		if(currLeaves>carryOverLeavesSick){
			return currLeaves-carryOverLeavesSick;
		}else{
			return 0;
		}
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		List<Employee> empList = EmployeeDAO.getInstance().getEmployees();
		for(Employee e1 : empList){
			int carriedOverCasual = removeFromCarryOverCasual(e1.getCasualLeaveBalance()); 
			int carriedOverSick = removeFromCarryOverSick(e1.getSickLeaveBalance()); 
			EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "casual", carriedOverCasual, true);
			EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "sick", carriedOverSick, true);
		}
		
	}	
}
