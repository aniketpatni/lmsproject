package snapdeal.lms.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import snapdeal.lms.entityDAO.Password;
import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.entityDAO.RoleEmpMap;
import snapdeal.lms.entityDAO.RoleEmpMapDAO;

import antlr.collections.List;

public class Validation {
	public static String encryption(String password)
	{
	SecureRandom random = new SecureRandom();
	MessageDigest encrypt=null;
	try {
		encrypt = MessageDigest.getInstance("SHA1");
	encrypt.reset();
		encrypt.update(password.getBytes("UTF-8"));
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String salt = "";
	for(int i=0;i<4;i++)
		{salt=salt+(char)(random.nextInt(255)+1);
		}
	byte[] digest = encrypt.digest();
	String encryptPass = "";
	for (int i = 0; i < digest.length; i++) {
		encryptPass +=  Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring( 1 );
	}
	return salt+encryptPass;	
	}
	
	public static ArrayList<String> roles(String empid){
		//RoleEmpMapDAO role= new RoleEmpMapDAO();
		ArrayList<String> arr= new ArrayList<String>();
		java.util.List<RoleEmpMap> roles=RoleEmpMapDAO.listRolesByEmpId(Integer.parseInt(empid));
		for(RoleEmpMap role : roles){
			arr.add(role.getRole().getRoles());
		}
//		System.out.println(arr.toString());
		return arr;
	}
	
	public static boolean passwordCheck(String empid, String password){
		PasswordDAO pass= new PasswordDAO();
		java.util.List<Password> idlist=pass.listPassword();
		for (Password passw : idlist){
//			System.out.println(passw.getEmp().getEmpId());
		if(new Integer(passw.getId()).toString().equals(empid)){
			if(pass.listPasswordByEmpId(Integer.parseInt(empid)).equals(Validation.encryption(password).substring(4)))
				return true;
			else
				return false;
		}
	}
		return false;}
}
