package snapdeal.lms.util;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SchedulerMetaData;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CronTriggers implements ServletContextListener{

	public static void run() throws Exception {

		Logger log = LoggerFactory.getLogger(CronTriggers.class);

		log.info("------- Initializing -------------------");

		//Get the default scheduler factory from context variable. 
		
		// First we must get a reference to a scheduler
		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sched = sf.getScheduler();

		log.info("------- Initialization Complete --------");

		log.info("------- Scheduling Jobs ----------------");

		// jobs can be scheduled before sched.start() has been called

		
		
		// job 1 will run at the start of every quarter at 00:05 am
		JobDetail job = newJob(UpdateLeavesQuarterlyJob.class)
				.withIdentity("job1", "group1")
				.build();

		CronTrigger trigger = newTrigger().withIdentity("trigger1", "group1")
				.withSchedule(cronSchedule("0 10 0 1 1/3 ?"))
				.build();

		sched.scheduleJob(job, trigger);
		
//		Date ft = sched.scheduleJob(job, trigger);
//		log.info(job.getKey() + " has been scheduled to run at: " + ft
//				+ " and repeat based on expression: "
//				+ trigger.getCronExpression());

		
		
		// job 2 will run at the start of every year at 00:02 am of Jan 01
		job = newJob(UpdateLeavesYearlyJob.class)
				.withIdentity("job2", "group1")
				.build();

		trigger = newTrigger().withIdentity("trigger2", "group1")
				.withSchedule(cronSchedule("0 2 0 1 1 ?"))
				.build();

		sched.scheduleJob(job, trigger);
		
//		ft = sched.scheduleJob(job, trigger);
//		log.info(job.getKey() + " has been scheduled to run at: " + ft
//				+ " and repeat based on expression: "
//				+ trigger.getCronExpression());

		
		
		// job 3 will run every night at 00:10 am 
		job = newJob(LeaveExpiryJobs.class)
				.withIdentity("job1", "group2")
				.build();

		trigger = newTrigger().withIdentity("trigger3", "group1")
				.withSchedule(cronSchedule("0 10 0 * * ?"))
				.build();

		sched.scheduleJob(job, trigger);
		
		log.info(job.getKey()+"has been scheduled. Lets hope leaves have expired :D");
		
//		ft = sched.scheduleJob(job, trigger);
//		log.info(job.getKey() + " has been scheduled to run at: " + ft
//				+ " and repeat based on expression: "
//				+ trigger.getCronExpression());

		log.info("------- Starting Scheduler ----------------");

		// All of the jobs have been added to the scheduler, but none of the
		// jobs will run until the scheduler has been started
		sched.start();
//
//		log.info("------- Started Scheduler -----------------");
//
//		log.info("------- Waiting five minutes... ------------");
//		try {
//			// wait five minutes to show jobs
//			Thread.sleep(300L * 1000L);
//			// executing...
//		} catch (Exception e) {
//		}
//
//		log.info("------- Shutting Down ---------------------");
//
//		sched.shutdown(true);
//
//		log.info("------- Shutdown Complete -----------------");
//
//		SchedulerMetaData metaData = sched.getMetaData();
//		log.info("Executed " + metaData.getNumberOfJobsExecuted() + " jobs.");
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			CronTriggers.run();
		} catch (Exception e){
			e.printStackTrace();
		}		
	}
}
