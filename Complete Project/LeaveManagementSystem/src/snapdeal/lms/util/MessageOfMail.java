package snapdeal.lms.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MessageOfMail {

	public String getMessageContent(String link, String empname, String code)
	{
		String filename= "passwordmailsubject";
		//File file = new File("./WebContent/" + filename + ".html");

		java.net.URL url = MessageOfMail.class.getClassLoader().getResource(""+filename+".html");
		String file = url.getPath();
		
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(file);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		@SuppressWarnings("resource")
		BufferedReader buf = new BufferedReader(fileReader);

		String mailContent = new String();
		String str = "";

		try {
			while ((str = buf.readLine()) != null) {
				str = str.replace("LINK",
						(CharSequence) link);
				str = str.replace("EMPLOYEENAME",
						(CharSequence) empname);
				str = str.replace("CODE", code);
				mailContent = mailContent + str;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mailContent;
	}
	
	@SuppressWarnings("rawtypes")
	public String getMessageContent(String prevState, String currentState,
			String SMH, HashMap mailDetails) {

		String content = null;

		/*
		 * case 1st | null| pending_approval_by_manager
		 */
		if (prevState.contains("null")
				&& currentState.contains("pending_approval_by_manager")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent("nulltobeApprovedtoEmployee",
						mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("nulltobeApprovedtoManager",
						mailDetails);
			}
		}

		/*
		 * case 2nd | pending_approval_by_manager | approved_by_manager
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("approved_by_manager")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent(
						"approvedbyManagertoEmuployee", mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("approvedbyManagertoManager",
						mailDetails);
			} else if (SMH.equals("H")) {
				content = this.setmessagecontent("approvedbyManagertoHR",
						mailDetails);
			}
		}

		/*
		 * case 3rd | pending_approval_by_manager | rejected_by_manager
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("rejected_by_manager")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent("rejecetdbyManagertoEmployee",
						mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("rejectedbyManagertoManager",
						mailDetails);
			}
		}

		/*
		 * case 4 | pending_approval_by_manager | expired_by_date
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("expired_by_date")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent("expiredtoEmployee",
						mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("expiredtoManager",
						mailDetails);
			}
		}

		/*
		 * case 5 | pending_approval_by_manager | deleted_by_employee
		 */

		if (prevState.contains("pending_approval_by_manager")
				&& currentState.contains("deleted_by_employee")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent(
						"tobeApprovedDeletedtoEmployee", mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent(
						"tobeApprovedDeletedtoManager", mailDetails);
			}
		}

		/*
		 * case 6 | approved_by_manager | deleted_by_employee
		 */

		if (prevState.contains("approved_by_manager")
				&& currentState.contains("deleted_by_employee")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent("approvedDeletedtoEmployee",
						mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("approvedDeletedtoManager",
						mailDetails);
			} else if (SMH.equals("H")) {
				content = this.setmessagecontent("approvedDeletedtoHR",
						mailDetails);
			}
		}
		
		/*
		 * case 7
		 */
		if (prevState.contains("deleted_by_employee")
				&& currentState.contains("pending_approval_by_manager")) {
			if (SMH.equals("S")) {
				content = this.setmessagecontent("updatedtoEmployee",
						mailDetails);
			} else if (SMH.equals("M")) {
				content = this.setmessagecontent("updatedtoManager",
						mailDetails);
			}
		}

		return content;
	}

	@SuppressWarnings("rawtypes")
	public String setmessagecontent(String filename, HashMap mailDetails) {
//		File file = new File("./" + filename + ".html");
		java.net.URL url = MessageOfMail.class.getClassLoader().getResource(""+filename+".html");
		String file = url.getPath();
//		System.out.println(filePath);
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(file);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		@SuppressWarnings("resource")
		BufferedReader buf = new BufferedReader(fileReader);

		String mailContent = new String();
		String str = "";

		try {
			while ((str = buf.readLine()) != null) {
				str = str.replace("EMPLOYEENAME",
						(CharSequence) mailDetails.get("empName"));
				str = str.replace("EMPLOYEEID",
						(CharSequence) ((Integer) mailDetails.get("empId"))
								.toString());
				str = str.replace("LEAVEID",
						(CharSequence) ((Integer) mailDetails.get("leaveId"))
								.toString());
				str = str.replace("MANAGERNAME",
						(CharSequence) mailDetails.get("managerName"));
				str = str.replace("APPLIEDON", mailDetails.get("appliedOn")
						.toString());
				str = str.replace("LEAVEFROM", mailDetails.get("leaveFrom")
						.toString());
				str = str.replace("LEAVETO", mailDetails.get("leaveTo")
						.toString());
				str = str.replace("LEAVEREASON",
						(CharSequence) mailDetails.get("leaveReason"));
				str = str.replace("REMARKS",
						(CharSequence) mailDetails.get("remarks"));
				str = str.replace("CASUALLEAVEBALANCE",
						(CharSequence) ((Integer) mailDetails
								.get("casualLeaveBalance")).toString());
				str = str.replace("SICKLEAVEBALANCE",
						(CharSequence) ((Integer) mailDetails
								.get("sickLeaveBalance")).toString());
				str = str.replace("DURATIONOFDAY",
						(CharSequence) ((Long) mailDetails.get("duration"))
								.toString());

				mailContent = mailContent + str;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mailContent;
	}

}
