package snapdeal.lms.servlet;

import java.io.IOException;
import snapdeal.lms.entityDAO.*;
import snapdeal.lms.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserLogin() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String empId, password;
		empId = request.getParameter("employee_id");
		password = request.getParameter("password");
		if(request.getSession().getAttribute("error") != null){
			request.getSession().removeAttribute("error");
		}
		if(request.getSession().getAttribute("messages") != null){
			request.getSession().removeAttribute("messages");
		}
		if (empId != null && empId != ""
				&& Validation.passwordCheck(empId, password)) {
			request.getSession().setAttribute("error", null);
			int emp_Id = Integer.parseInt(empId);
			Employee emp = EmployeeDAO.getInstance().getEmployee(emp_Id);
			request.getSession().setAttribute("emp", emp);
		
			if (Validation.roles(empId).contains("HR")) {
				if(Validation.roles(empId).contains("Manager"))
					request.getSession().setAttribute("role", "hrmanager");
				else
					request.getSession().setAttribute("role", "hr");
				response.sendRedirect(("./employee/createleave.jsp"));
			}
			else if (Validation.roles(empId).contains("Manager")) {
				request.getSession().setAttribute("role", "manager");
				response.sendRedirect(("./employee/createleave.jsp"));
			} else if (Validation.roles(empId).contains("Employee")) {
				request.getSession().setAttribute("role", "employee");
				response.sendRedirect(("./employee/createleave.jsp"));
			}

		} else {
			request.getSession().setAttribute("error", "Invalid Login");
			response.sendRedirect("login.jsp");
		}

	}
}