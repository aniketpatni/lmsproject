package snapdeal.lms.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.EmployeeDAO;
import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.util.Validation;

/**
 * Servlet implementation class AddEmployee
 */
public class AddEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			int empId = Integer.parseInt(request.getParameter("empId"));
			String name = request.getParameter("empName");
			String dept = request.getParameter("empDept");
			String emailId = request.getParameter("empEmail");
			Date dateOfJoining =Date.valueOf(request.getParameter("joiningDate"));
			int managerId = Integer.parseInt(request.getParameter("managerId"));
			String designation = request.getParameter("designation");
			int sickLeaveBalance = Integer.parseInt(request.getParameter("sickLeaves"));
			int casualLeaveBalance = Integer.parseInt(request.getParameter("casualLeaves"));
			String empStatus = request.getParameter("status");
			EmployeeDAO.getInstance().addEmployee(empId, name, dept, emailId, dateOfJoining, managerId , designation, sickLeaveBalance, casualLeaveBalance, empStatus, 1);
			new PasswordDAO().savePassword(empId, Validation.encryption(name).substring(4));
			request.getRequestDispatcher("./EmployeeAddedSuccessfully.jsp").forward(request, response);
		}catch(Exception e){
			request.getRequestDispatcher("./EmployeeNotAddedSuccessfully.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			int empId = Integer.parseInt(request.getParameter("empId"));
			String name = request.getParameter("empName");
			String dept = request.getParameter("empDept");
			String emailId = request.getParameter("empEmail");
			Date dateOfJoining = new Date(new java.util.Date().getTime());
			int managerId = Integer.parseInt(request.getParameter("managerId"));
			String designation = request.getParameter("designation");
			int sickLeaveBalance = Integer.parseInt(request.getParameter("sickLeaves"));
			int casualLeaveBalance = Integer.parseInt(request.getParameter("casualLeaves"));
			String empStatus = request.getParameter("status");
			EmployeeDAO.getInstance().addEmployee(empId, name, dept, emailId, dateOfJoining, managerId , designation, sickLeaveBalance, casualLeaveBalance, empStatus, 1);
			new PasswordDAO().savePassword(empId, Validation.encryption(name).substring(4));
			request.getRequestDispatcher("./EmployeeAddedSuccessfully.jsp").forward(request, response);
		}catch(Exception e){
			request.getRequestDispatcher("./EmployeeNotAddedSuccessfully.jsp").forward(request, response);
		}
	}

}
