package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.util.Validation;

import snapdeal.lms.entityDAO.ResetPasswordClass;
import snapdeal.lms.entityDAO.ResetPasswordClassDAO;

/**
 * Servlet implementation class CodeCheck
 */
public class CodeCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CodeCheck() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("error") != null){
			request.getSession().removeAttribute("error");
		}
		if(request.getSession().getAttribute("messages") != null){
			request.getSession().removeAttribute("messages");
		}
		String empId, code;
		boolean flag = false;
		empId = request.getParameter("empId");
		request.getSession().setAttribute("empId", empId);
		code = request.getParameter("code");
		String extension = Validation.encryption(empId + code).substring(4);
		int extensionId=(Integer) request.getSession().getAttribute("extensionId");
		ResetPasswordClassDAO pass = new ResetPasswordClassDAO();
//		List<ResetPasswordClass> idlist = pass.listResetPasswordClass();
		String codew= pass.listResetPasswordById(extensionId);
		if(codew!=null){
			if (extension.equals(codew)) {
				flag = true;
			}
		}
		if (flag) {
			pass.deleteResetPasswordClass(extensionId);
			request.getSession().removeAttribute("extensionId");
			response.sendRedirect("ResetPassword.jsp");
		} else {
			request.getSession().setAttribute("messages", "invalid code");
			response.sendRedirect("forgotPassword.jsp");
		}
	}

}
