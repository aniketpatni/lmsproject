package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;

/**
 * Servlet implementation class Leave
 */
public class Leave extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Leave() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("./employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int empId = ((Employee) request.getSession().getAttribute("emp"))
				.getEmpId();
		int leaveId = Integer.parseInt(request.getParameter("leave_id"));
		LeaveRequest leave = LeaveRequestDAO.getInstance()
				.getLeaveRequestByLeaveId(leaveId);
		if (leave.getEmp().getEmpId() == empId) {
				request.setAttribute("leave", leave);
				RequestDispatcher rd = request
						.getRequestDispatcher("./updateleave.jsp");
				rd.forward(request, response);
		} else {
			request.getSession().setAttribute("message", "4");
			response.sendRedirect("./unsuccessful.jsp");
		}
	}
}
