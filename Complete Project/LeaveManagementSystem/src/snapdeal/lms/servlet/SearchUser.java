package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;

/**
 * Servlet implementation class SearchUserFunctionality1
 */
public class SearchUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public SearchUser() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId=Integer.parseInt(request.getParameter("empId"));
		try{
			Employee employeeObject =  EmployeeDAO.getInstance().getEmployee(empId);
			request.setAttribute("employeeObject", employeeObject);
			request.getRequestDispatcher("./EmployeeDetail.jsp").forward(request, response);
		}catch(Exception e){
			request.getRequestDispatcher("./employeetask.html").forward(request, response);
		}
	}

}
