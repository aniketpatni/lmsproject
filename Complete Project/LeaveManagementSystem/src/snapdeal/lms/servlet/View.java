package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;



/**
 * Servlet implementation class View
 */
public class View extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public View() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int empId = ((Employee)request.getSession().getAttribute("emp")).getEmpId();
		List<LeaveRequest> leaveRequests = LeaveRequestDAO.getInstance().getLeaveRequestsByEmpId(empId);
		request.setAttribute("leave", leaveRequests);
		RequestDispatcher rd = request.getRequestDispatcher("./leavehistory.jsp");
		rd.forward(request, response);
	}

}
