package snapdeal.lms.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.OfficialHolidaysDAO;

/**
 * Servlet implementation class AddHolidaysOneByOne
 */
public class AddHolidaysOneByOne extends HttpServlet {
	
	private static final long serialVersionUID = 7719809431228114371L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public AddHolidaysOneByOne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String date = request.getParameter("date");
		String holiday = request.getParameter("holiday");
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			holidayDAO.saveHoliday(holiday, (Date) dateFormat.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e){
			System.out.println("Could not add man :(");
		}finally{
			response.sendRedirect("./OutputHolidayList");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String date = request.getParameter("date");
		String holiday = request.getParameter("holiday");
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		try {
			holidayDAO.saveHoliday(holiday, (Date) dateFormat.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e){
			System.out.println("Could not add man :(");
		} finally {
			response.sendRedirect("./OutputHolidayList");
		}
	}

}
