package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;

/**
 * Servlet implementation class AcceptRequestServlet
 */
public class AcceptRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AcceptRequestServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("../employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			int leaveId = Integer.parseInt(request.getParameter("leaveId"));
			LeaveRequestDAO leaveRequestDAO = LeaveRequestDAO.getInstance();
			LeaveRequest leaveRequest = (LeaveRequest) leaveRequestDAO
					.getLeaveRequestByLeaveId(leaveId);
			request.setAttribute("individualRequest", leaveRequest);
			RequestDispatcher reqDispatcher = getServletContext()
					.getRequestDispatcher("/manager/ViewIndividualDetails.jsp");
			reqDispatcher.forward(request, response);
		} catch (Exception e) {
			response.sendRedirect("../employee/createleave.jsp");
		}
	}

}
