package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.entityDAO.LeaveStateLog;
import snapdeal.lms.entityDAO.LeaveStateLogDAO;

/**
 * Servlet implementation class ViewLog
 */
public class ViewLog extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewLog() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("./employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		int empId = ((Employee) request.getSession().getAttribute("emp"))
				.getEmpId();
		int leaveId = Integer.parseInt(request.getParameter("leave_id"));
		LeaveRequest leave = LeaveRequestDAO.getInstance()
				.getLeaveRequestByLeaveId(leaveId);
		if (leave.getEmp().getEmpId() == empId) {
			List<LeaveStateLog> leaveLogs = LeaveStateLogDAO.getInstance()
					.getHistory(leaveId);
			request.setAttribute("leavelogs", leaveLogs);
			RequestDispatcher rd = request
					.getRequestDispatcher("./viewlog.jsp");
			rd.forward(request, response);
		} else {
			request.getSession().setAttribute("message", "4");
			response.sendRedirect("./unsuccessful.jsp");
		}
	}

}
