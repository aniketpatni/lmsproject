package snapdeal.lms.servlet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.OfficialHolidaysDAO;

/**
 * Servlet implementation class AddHolidayByCSV
 */
public class AddHolidayByCSV extends HttpServlet {
	
	private static final long serialVersionUID = -8541832997990451689L;

	/**
     * @see HttpServlet#HttpServlet()
     */
    public AddHolidayByCSV() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		BufferedReader CSVFile = null;
		String dataRow = null;
		try{
			CSVFile = new BufferedReader(new FileReader(	"/home/user/apache-tomcat-6.0.35/webapps/data/Holidays.csv"));
			dataRow = CSVFile.readLine();
		}catch(Exception e){
			response.sendRedirect("./OutputHolidayList");
			return;
		}
		while (dataRow != null) {
			String[] dataArray = dataRow.split(",");
			try {
				holidayDAO.saveHoliday(dataArray[1],
						(Date) dateFormat.parse(dataArray[0]));
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (Exception e){
				System.out.println("Couldn't do it man :(");
			}
			dataRow = CSVFile.readLine();
		}
		
		try{
			CSVFile.close();
		} catch(Exception e){
			System.out.println("CSV file error !");
		} finally {
			response.sendRedirect("./OutputHolidayList");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		BufferedReader CSVFile = null;
		String dataRow = null;
		try{
			CSVFile = new BufferedReader(new FileReader(	"/home/user/apache-tomcat-6.0.35/webapps/data/Holidays.csv"));
			dataRow = CSVFile.readLine();
		}catch(Exception e){
			response.sendRedirect("./OutputHolidayList");
			return;
		}
		while (dataRow != null) {
			String[] dataArray = dataRow.split(",");
			try {
				holidayDAO.saveHoliday(dataArray[1],
						(Date) dateFormat.parse(dataArray[0]));
			} catch (ParseException e) {
				e.printStackTrace();
			}catch (Exception e){
				System.out.println("Couldn't do it man :(");
			}
			dataRow = CSVFile.readLine();
		}
		try{
			CSVFile.close();
		} catch(Exception e){
			System.out.println("CSV file error !");
		} finally {
			response.sendRedirect("./OutputHolidayList");
		}
		
	}

}
