package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Password;
import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.entityDAO.ResetPasswordClass;
import snapdeal.lms.entityDAO.ResetPasswordClassDAO;
import snapdeal.lms.util.Validation;

/**
 * Servlet implementation class ResetPassword
 */
public class ResetPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResetPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getSession().getAttribute("error") != null){
			request.getSession().removeAttribute("error");
		}
		if(request.getSession().getAttribute("messages") != null){
			request.getSession().removeAttribute("messages");
		}
		boolean flag = false;
		ResetPasswordClassDAO pass = new ResetPasswordClassDAO();
		List<ResetPasswordClass> idlist = pass.listResetPasswordClass();
		for (ResetPasswordClass passw : idlist) {
			if ((request.getRequestURI().contains(passw.getExtension()))) {
				flag = true;
				request.getSession().setAttribute("extensionId", passw.getId());
				break;
			}
		}
		if (!flag) {
			request.getSession().setAttribute("messages", "invalid link");
			if(request.getRequestURI().contains("resetpassword/")){
				response.sendRedirect("../forgotPassword.jsp");
			}else if(request.getRequestURI().contains("resetpassword")){
				response.sendRedirect("./forgotPassword.jsp");
			}
		} else {
			response.sendRedirect("../enterCode.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
