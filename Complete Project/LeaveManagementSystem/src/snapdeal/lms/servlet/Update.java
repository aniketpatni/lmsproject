package snapdeal.lms.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.util.DateValidation;
import snapdeal.lms.util.NoOfLeaveDays;
import snapdeal.lms.util.SendMail;

/**
 * Servlet implementation class Update
 */
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("./employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int empId = ((Employee) request.getSession().getAttribute("emp"))
				.getEmpId();
		int leaveId = Integer.parseInt(request.getParameter("leave_id"));
		LeaveRequest leave = LeaveRequestDAO.getInstance()
				.getLeaveRequestByLeaveId(leaveId);
		if (leave.getEmp().getEmpId() == empId) {
			LeaveRequestDAO.getInstance().updateLeaveRequestState(leaveId,
					"deleted_by_employee", "Deleted for Updating", empId);
			if (DateValidation.LeaveCheck(request)) {
				Date startDate = Date.valueOf(request
						.getParameter("leave_from"));
				Date endDate = Date.valueOf(request.getParameter("leave_to"));
				String state = "pending_approval_by_manager";
				String leaveType = request.getParameter("leave_type");
				String leaveReason = request.getParameter("leave_reason");
				int noOfLeaveDays = NoOfLeaveDays.getNoOfLeaveDays(startDate,
						endDate);
				if (noOfLeaveDays == 0) {
					request.getSession().setAttribute("message", "1");
					LeaveRequestDAO.getInstance().updateLeaveRequestState(
							leaveId, "pending_approval_by_manager",
							"Tried to select holidays", empId);
					response.sendRedirect("./unsuccessful.jsp");
				} else {
					LeaveRequestDAO.getInstance().updateLeaveRequest(leaveId,
							empId, startDate, endDate, state, leaveType,
							leaveReason, noOfLeaveDays);
					SendMail.getInstance().send(leaveId);
					response.sendRedirect("./showhistory");
				}
			} else {
				request.getSession().setAttribute("message", "2");
				LeaveRequestDAO.getInstance().updateLeaveRequestState(leaveId,
						"pending_approval_by_manager",
						"Tried to overlap holidays", empId);
				response.sendRedirect("./unsuccessful.jsp");
			}
		} else {
			request.getSession().setAttribute("message", "4");
			response.sendRedirect("./unsuccessful.jsp");
		}
	}
}