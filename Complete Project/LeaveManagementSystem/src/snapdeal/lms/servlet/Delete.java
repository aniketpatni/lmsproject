package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.util.SendMail;

/**
 * Servlet implementation class Delete
 */
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Delete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("./employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int empId = ((Employee) request.getSession().getAttribute("emp"))
				.getEmpId();
		int leaveId = Integer.parseInt(request.getParameter("leave_id"));
		LeaveRequest leave = LeaveRequestDAO.getInstance()
				.getLeaveRequestByLeaveId(leaveId);
		if (leave.getEmp().getEmpId() == empId) {
			LeaveRequestDAO.getInstance().updateLeaveRequestState(leaveId,
					"deleted_by_employee", "deleted", empId);
			SendMail.getInstance().send(leaveId);
			response.sendRedirect("./showhistory");
		} else {
			request.getSession().setAttribute("message", "4");
			response.sendRedirect("./unsuccessful.jsp");
		}
	}

}
