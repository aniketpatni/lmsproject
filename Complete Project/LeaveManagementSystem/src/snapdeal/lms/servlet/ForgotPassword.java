package snapdeal.lms.servlet;

import java.io.IOException;
import java.security.SecureRandom;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;
import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.entityDAO.ResetPasswordClassDAO;
import snapdeal.lms.util.SendMail;
import snapdeal.lms.util.Validation;

/**
 * Servlet implementation class ForgotPassword
 */
public class ForgotPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ForgotPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	public static String generateCode() {
		String password = "";
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < 8; i++) {
			password = password + (char) (random.nextInt(26) + 97);
		}
		return password;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getSession().getAttribute("error") != null) {
			request.getSession().removeAttribute("error");
		}
		if (request.getSession().getAttribute("messages") != null) {
			request.getSession().removeAttribute("messages");
		}
		PasswordDAO pass = new PasswordDAO();
		String empId;
		empId = request.getParameter("emp_id");
		EmployeeDAO empDao = EmployeeDAO.getInstance();

		try {
			if (empDao.getEmployee(Integer.parseInt(empId)) != null) {
				String code = ForgotPassword.generateCode();
				String link = "http://localhost:8080/LeaveManagementSystem/resetpassword/"
						+ Validation.encryption(empId + code);
				SendMail.getInstance().send(empId, link, code);
				ResetPasswordClassDAO.addResetPasswordClass(Validation
						.encryption(empId + code).substring(4));
				request.getSession().setAttribute("messages",
						"check your mail for further process");
				response.sendRedirect("./login.jsp");
			} else {
				request.getSession().setAttribute("messages",
						"invalid employee id");
				response.sendRedirect("./forgotPassword.jsp");
			}
		} catch (Exception e) {
			request.getSession()
					.setAttribute("messages", "invalid employee id");
			response.sendRedirect("./forgotPassword.jsp");
		}
	}
}
