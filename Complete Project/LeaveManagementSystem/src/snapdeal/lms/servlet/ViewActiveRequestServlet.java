package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;

/**
 * Servlet implementation class ViewActiveRequestServlet
 */
public class ViewActiveRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ViewActiveRequestServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Employee employee = (Employee) session.getAttribute("emp");
		LeaveRequestDAO leaveRequestDAO = LeaveRequestDAO.getInstance();
		List<LeaveRequest> leaveRequestList = leaveRequestDAO
				.getLeaveRequestByManagerIdByState(employee.getEmpId(),
						"active");
		RequestDispatcher reqDispatcher = null;
		if (leaveRequestList.size() != 0) {
			request.setAttribute("leaveRequestList", leaveRequestList);
			reqDispatcher = getServletContext().getRequestDispatcher(
					"/manager/viewactiverequest.jsp");
		} else {
			reqDispatcher = getServletContext().getRequestDispatcher(
					"/manager/norecordfound.jsp");
		}
		reqDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
