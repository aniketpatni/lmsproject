package snapdeal.lms.servlet;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.util.DateValidation;
import snapdeal.lms.util.NoOfLeaveDays;
import snapdeal.lms.util.SendMail;

/**
 * Servlet implementation class Add
 */
public class Add extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Add() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("./employee/error.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (DateValidation.LeaveCheck(request)) {
			// TODO Auto-generated method stub
			int empId = ((Employee) request.getSession().getAttribute("emp"))
					.getEmpId();
			Date startDate = Date.valueOf(request.getParameter("leave_from"));
			Date endDate = Date.valueOf(request.getParameter("leave_to"));
			String state = "pending_approval_by_manager";
			String leaveType = request.getParameter("leave_type");
			String leaveReason = request.getParameter("leave_reason");
//			if (startDate.before(endDate)) {
				int noOfLeaveDays = NoOfLeaveDays.getNoOfLeaveDays(startDate,
						endDate);
				if (noOfLeaveDays == 0) {
					request.getSession().setAttribute("message", "1");
					response.sendRedirect("./unsuccessful.jsp");
				} else {
					int leaveId = LeaveRequestDAO.getInstance()
							.addLeaveRequest(empId, startDate, endDate, state,
									leaveType, leaveReason, noOfLeaveDays);
					if (leaveId != 0) {
						SendMail.getInstance().send(leaveId);
						request.getSession().setAttribute("message", "Successful");
						response.sendRedirect("./thankyou.jsp");
					} else {
						request.getSession().setAttribute("message", "3");
						response.sendRedirect("./unsuccessful.jsp");
					}
				}
			} else {
				request.getSession().setAttribute("message", "2");
				response.sendRedirect("./unsuccessful.jsp");
			}
//		} else {
//			request.getSession().setAttribute("message", "5");
//			response.sendRedirect("./unsuccessful.jsp");
//		}
	}
}
