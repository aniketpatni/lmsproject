package snapdeal.lms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.util.SendMail;

/**
 * Servlet implementation class ActiveRequestActionServlet
 */
public class ActiveRequestActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ActiveRequestActionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			RequestDispatcher reqDispatcher = null;
			HttpSession session = request.getSession();
			LeaveRequestDAO leaveRequestDAO = LeaveRequestDAO.getInstance();
			Employee employee = (Employee) session.getAttribute("emp");
			int leaveId = Integer.parseInt(request.getParameter("leaveId"));
			List<LeaveRequest> leaveRequestList = leaveRequestDAO
					.getLeaveRequestByManagerIdByState(employee.getEmpId(), "state");
			boolean flag = false;
			for (LeaveRequest leaveRequest : leaveRequestList) {
				if (leaveRequest.getLeaveId() == leaveId) {
					flag = true;
					break;
				}
			}
			if (flag) {
				String state = request.getParameter("action");
				if ("accept".equals(state)) {
					state = "approved_by_manager";
					request.setAttribute("status", "Request Accepted");
				} else if ("reject".equals(state)) {
					state = "rejected_by_manager";
					request.setAttribute("status", "Request Rejected");
				}
				String remarks = request.getParameter("remarks");
				LeaveRequest leaveRequest = (LeaveRequest) leaveRequestDAO
						.getLeaveRequestByLeaveId(leaveId);
				if ("pending_approval_by_manager".equals(leaveRequest
						.getState().getState())) {
					leaveRequestDAO.updateLeaveRequestState(leaveId, state,
							remarks, employee.getEmpId());
					SendMail.getInstance().send(leaveId);
					EmployeeDAO employeeDAO = null;
					if ("approved_by_manager".equals(state)) {
						employeeDAO = EmployeeDAO.getInstance();
						employeeDAO.updateEmpLeaves(leaveRequest.getEmp()
								.getEmpId(), leaveRequest.getLeaveType()
								.getLeaveType(), leaveRequest
								.getNoOfLeaveDays(), true);
					}
					reqDispatcher = getServletContext().getRequestDispatcher(
							"/manager/actionstatus.jsp");
				} else {
					reqDispatcher = getServletContext().getRequestDispatcher(
							"/manager/error.jsp");
				}
				reqDispatcher.forward(request, response);
			}else {
				response.sendRedirect("../employee/error.jsp");
			}
		} catch (Exception e) {
			response.sendRedirect("../employee/error.jsp");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("../employee/error.jsp");
	}

}
