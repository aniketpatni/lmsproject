package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.DesignationLeaveMapDAO;

/**
 * Servlet implementation class MaxLeaveServlet
 */
public class SetMaxLeaveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetMaxLeaveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String designation = request.getParameter("designation");
		String sickLeave = request.getParameter("sickLeaves");
		String casualLeave = request.getParameter("casualLeaves");
		try{
			DesignationLeaveMapDAO leaves = new DesignationLeaveMapDAO();
			leaves.setCasualLeavesByDesignation(designation, Integer.parseInt(casualLeave));
			leaves.setSickLeavesByDesignation(designation, Integer.parseInt(sickLeave));
			request.getRequestDispatcher("./LeaveSetSuccess.jsp").forward(request, response);
		} catch(Exception e){
			System.out.println("Sorry could not change leaves");
			request.getRequestDispatcher("./LeaveSetFailure.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String designation = request.getParameter("designation");
		String sickLeave = request.getParameter("sickLeaves");
		String casualLeave = request.getParameter("casualLeaves");
		try{
			DesignationLeaveMapDAO leaves = new DesignationLeaveMapDAO();
			leaves.setCasualLeavesByDesignation(designation, Integer.parseInt(casualLeave));
			leaves.setSickLeavesByDesignation(designation, Integer.parseInt(sickLeave));
			request.getRequestDispatcher("./LeaveSetSuccess.jsp").forward(request, response);
		} catch(Exception e){
			System.out.println("Sorry could not change leaves");
			request.getRequestDispatcher("./LeaveSetFailure.jsp").forward(request, response);
		}
		
	}

}
