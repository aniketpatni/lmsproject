package snapdeal.lms.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.util.Validation;

/**
 * Servlet implementation class UpdatePassword
 */
public class UpdatePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdatePassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		int empId = ((Employee) request.getSession().getAttribute("emp")).getEmpId();
		PasswordDAO pass= new PasswordDAO();
		String newPassword,oldPassword;
		oldPassword = request.getParameter("oldpass");
		newPassword = request.getParameter("cpass");
		
		if(pass.listPasswordByEmpId(empId).equals(Validation.encryption(oldPassword).substring(4))){
			pass.updatePassword(empId, Validation.encryption(newPassword).substring(4));
			request.getSession().setAttribute("error", "Password was successfully changed");
			response.sendRedirect("../login.jsp");
		}
		else{
			request.getSession().setAttribute("messages", "Invalid Submission");
			response.sendRedirect("updatePassword.jsp");
		}
	}

}
