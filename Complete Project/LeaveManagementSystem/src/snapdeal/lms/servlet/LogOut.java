package snapdeal.lms.servlet;

import java.io.IOException;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogOut
 */
public class LogOut extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogOut() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getSession().removeAttribute("emp");
		request.getSession().removeAttribute("role");
		request.getSession().removeAttribute("error");
		if(request.getSession().getAttribute("messages")!= null){
			request.getSession().removeAttribute("messages");
		}
		if(request.getSession().getAttribute("extensionId")!= null){
			request.getSession().removeAttribute("extensionId");
		}
		if(request.getSession().getAttribute("empId")!= null){
			request.getSession().removeAttribute("empId");
		}
		if(request.getSession().getAttribute("message")!= null){
			request.getSession().removeAttribute("message");
		}
		request.getSession().invalidate();
		response.sendRedirect("./login.jsp");
	}
}
