package snapdeal.lms.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.PasswordDAO;
import snapdeal.lms.util.Validation;

/**
 * Servlet implementation class ChangePassword
 */
public class ChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangePassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("error") != null){
			request.getSession().removeAttribute("error");
		}
		if(request.getSession().getAttribute("messages") != null){
			request.getSession().removeAttribute("messages");
		}
		PasswordDAO pass = new PasswordDAO();
		String password;
		password = request.getParameter("cpass");
		pass.updatePassword(Integer.parseInt(request.getSession()
				.getAttribute("empId").toString()),
				Validation.encryption(password).substring(4));
		if(request.getSession().getAttribute("empId")!= null){
			request.getSession().removeAttribute("empId");
		}
		request.getSession().setAttribute("messages",
				"your password has been sucessfully changed");
		response.sendRedirect("login.jsp");
	}
}
