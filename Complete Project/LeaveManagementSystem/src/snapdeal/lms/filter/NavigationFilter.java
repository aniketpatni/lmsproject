package snapdeal.lms.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class NavigationFilter
 */
public class NavigationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public NavigationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		boolean check1 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/showhistory");
		boolean check2 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/thankyou.jsp");
		boolean check3 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/unsuccessful.jsp");
		boolean check4 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/updateleave");
		boolean check5 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/viewlog");
		boolean check6 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/createleave.jsp");
		boolean check7 = req.getRequestURI().equals(
				"/LeaveManagementSystem/forgotPassword.jsp");
		boolean check8 = req.getRequestURI().contains(
				"/LeaveManagementSystem/resetpassword");
		boolean check9 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/error.jsp");
		boolean check10 = req.getRequestURI().equals(
				"/LeaveManagementSystem/changepassword");
		boolean check11 = req.getRequestURI().contains(
				"/LeaveManagementSystem/update");
		
		if(check1){
			req.setAttribute("navigation", "> employee > Leave History");
		}
		if(check2){
			req.setAttribute("navigation", "> employee > Thank You");
		}
		if(check3){
			req.setAttribute("navigation", "> employee > Unsuccessful");
		}
		if(check4){
			req.setAttribute("navigation", "> employee > Update History");
		}
		if(check5){
			req.setAttribute("navigation", "> employee > Leave Log");
		}
		if(check6){
			req.setAttribute("navigation", "> employee > New Leave");
		}
		if(check7){
			req.setAttribute("navigation", "> Forgot Password");
		}
		if(check8){
			req.setAttribute("navigation", "> Reset Password Step1");
		}
		if(check9){
			req.setAttribute("navigation", "> Error");
		}
		if(check10){
			req.setAttribute("navigation", "> Reset Password Step2");
		}
		if(check11){
			req.setAttribute("navigation", "> Update Password");
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
