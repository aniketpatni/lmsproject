package snapdeal.lms.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class EmployeeJSPFilter
 */
public class EmployeeJSPFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public EmployeeJSPFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		boolean check1 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/leavehistory.jsp");
		boolean check2 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/thankyou.jsp");
		boolean check3 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/unsuccessful.jsp");
		boolean check4 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/updateleave.jsp");
		boolean check5 = req.getRequestURI().equals(
				"/LeaveManagementSystem/employee/viewlog.jsp");
		if (check1 || check4 || check5) {
			res.sendRedirect("./createleave.jsp");
		} else if (check2 || check3) {
			if (req.getSession().getAttribute("message") == null) {
				res.sendRedirect("./createleave.jsp");
			} else {
				chain.doFilter(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
