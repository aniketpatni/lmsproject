<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reset Password - LMS</title>

<script type="text/javascript">
	function checkPassword() {

		var pass = document.forms["form"]["pass"].value;
		var cpass = document.forms["form"]["cpass"].value;
		if (pass != cpass) {
			alert("password doesn't match");
			return false;
		}

	}
</script>
</head>
<body style="display: block">
	<center>
		<div style="background-color: #F8F8F8; height: 85px; width: 1200px;">
			<img src="./images/snapdeal_logo_tagline.png"
				style="float: left; position: relative; margin-top: 20px" />
			<div
				style="color: #3366FF; font-size: 35px; position: relative; top: 20px;">Leave
				Management System</div>
		</div>
		<br>
		<br>
		<div style="text-align: left; width: 1200px">
			Leave Management System <%out.print(request.getAttribute("navigation")); %>
			</div><br />
		<div style="width: 1200px; align: right">


			<div
				style="float: right; margin-top: 50px; font-size: 100%; background-color: #E6E6FA; width: 300px; height: 270px">
				<h3>
					<a href="#"
						style="text-decoration: none; margin-left: 30px; margin-bottom: 20px"></a>
				</h3>
				<div style="margin-left: 10px" align="left">

					<div id="invalid" style="visibility: hidden"></div>
					<br>

					<form name="form" method="post" action="./changepassword"
						onsubmit="return checkPassword()"
						style="position: relative; top: -13px;">

						<div>
							<label> <strong>Enter New Password </strong>
							</label>
						</div>
						<div>
							<input type="password" name="pass" id="pass" size="25"
								style="position: relative; top: 10px" />
						</div>
						<br>
						<div>
							<label> <strong>Confirm Password </strong>
							</label>
						</div>
						<div>
							<input type="password" name="cpass" id="cpass" size="25"
								style="position: relative; top: 10px" />
						</div>
						<br>

						<div>
							<button type="submit"
								style="background-color: #0099FF; border-radius: 8px; color: white; width: 80px; height: 30px; font-size: 15px; position: relative; top: 20px">Submit</button>
						</div>
						<br>

					</form>
				</div>
			</div>
		</div>
	</center>
</body>
</html>