<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Log - LMS</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../style/styles.css">
<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
			Leave Management System <%out.print(request.getAttribute("navigation")); %>
			</div><br />
			<div>
				<ul>
					<li><a href="./createleave.jsp">New Leave Request</a></li>
					<li><a href="./showhistory">Show Own Leave History</a></li>
					<li id="manager1"><a href="../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../hr/admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2"><a href="../hr/employeetasks/employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div id="empDetails">
				<p>
					Employee ID:
					<c:out value="${emp.empId}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Employee Name:
					<c:out value="${emp.name}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Current Casual Leave Balance:
					<c:out value="${emp.casualLeaveBalance}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Current Sick Leave Balance:
					<c:out value="${emp.sickLeaveBalance}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</p>
			</div>
			<br /> <br /> <br />
			<div>
				<table width=100% align="center" cellpadding="5">
					<thead>
						<tr>
							<td width="75"><div align="center">Leave ID</div></td>
							<td width="200"><div align="center">Related Message</div></td>
							<td width="200"><div align="center">Previous Leave
									State</div></td>
							<td width="200"><div align="center">Current Leave
									State</div></td>
							<td width="200"><div align="center">Time of Change</div></td>
						</tr>
					</thead>
					<c:forEach var="i" items="${leavelogs}" varStatus="loopStatus">
						<tr>
							<td><c:out value="${i.leave.leaveId}" /></td>
							<td><c:out value="${i.leaveMsg}" /></td>
							<td><c:out value="${i.prevState.state}" /></td>
							<td><c:out value="${i.currState.state}" /></td>
							<td><c:out value="${i.timeOfChange}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</center>
</body>
</html>