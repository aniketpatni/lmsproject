<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create New Leave Request - LMS</title>
<link rel="stylesheet" href="../themes/base/jquery.ui.all.css">
<script src="../js/jquery-1.7.1.js"></script>
<script src="../js/jquery.ui.core.js"></script>
<script src="../js/jquery.ui.widget.js"></script>
<script src="../js/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../style/demos.css">
<script type="text/javascript">
	function dateCheck(formobj) {
		var from = document.getElementById("from").value;
		var to = document.getElementById("to").value;
		if (from > to) {
			alert("From Date is before of start date");
			return false;
		}
		return true;
	}
</script>
<script type="text/javascript">
	function formCheck(formobj) {
		// Enter name of mandatory fields
		var fieldRequired = Array("leave_from", "leave_to", "leave_reason");
		// Enter field description to appear in the dialog box
		var fieldDescription = Array("Start Date", "End Date", "Leave Reason");
		// dialog message
		var alertMsg = "Please complete the following fields:\n";
		var l_Msg = alertMsg.length;
		for ( var i = 0; i < fieldRequired.length; i++) {
			var obj = formobj.elements[fieldRequired[i]];
			if (obj) {
				switch (obj.type) {
				case "select-one":
					if (obj.selectedIndex == -1
							|| obj.options[obj.selectedIndex].text == "") {
						alertMsg += " - " + fieldDescription[i] + "\n";
					}
					break;
				case "select-multiple":
					if (obj.selectedIndex == -1) {
						alertMsg += " - " + fieldDescription[i] + "\n";
					}
					break;
				case "text":
				case "textarea":
					if (obj.value == "" || obj.value == null) {
						alertMsg += " - " + fieldDescription[i] + "\n";
					}
					break;
				default:
				}
				if (obj.type == undefined) {
					var blnchecked = false;
					for ( var j = 0; j < obj.length; j++) {
						if (obj[j].checked) {
							blnchecked = true;
						}
					}
					if (!blnchecked) {
						alertMsg += " - " + fieldDescription[i] + "\n";
					}
				}
			}
		}

		if (alertMsg.length == l_Msg) {
			return true;
		} else {
			alert(alertMsg);
			return false;
		}
	}
// -->
</script>

<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<script>
	$(function() {
		var dates = $("#from, #to")
				.datepicker(
						{
							defaultDate : "+1w",
							changeMonth : true,
							numberOfMonths : 2,
							onSelect : function(selectedDate) {
								var option = this.id == "from" ? "minDate"
										: "maxDate", instance = $(this).data(
										"datepicker"), date = $.datepicker
										.parseDate(
												instance.settings.dateFormat
														|| $.datepicker._defaults.dateFormat,
												selectedDate, instance.settings);
								dates.not(this).datepicker("option", option,
										date);
							}
						});
	});
	$(function() {
		$("#from").datepicker("option", "dateFormat", "yy-mm-dd");
		$("#to").datepicker("option", "dateFormat", "yy-mm-dd");

	});
</script>
<link rel="stylesheet" href="../style/styles.css">
<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
			Leave Management System <%out.print(request.getAttribute("navigation")); %>
			</div><br />
			<div>
				<ul>
					<li class="active"><a href="./createleave.jsp">New Leave
							Request</a></li>
					<li><a href="./showhistory">Show Own Leave History</a></li>
					<li id="manager1"><a href="../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../hr/admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2"><a href="../hr/employeetasks/employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div id="empDetails">
				<p>
					Employee ID:
					<c:out value="${emp.empId}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Employee Name:
					<c:out value="${emp.name}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Current Casual Leave Balance:
					<c:out value="${emp.casualLeaveBalance}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Current Sick Leave Balance:
					<c:out value="${emp.sickLeaveBalance}"></c:out>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</p>
			</div>
			<br /> <br /> <br />
			<div style="text-align: left;">
				<form action="addrequest" method="post"
					onsubmit="return formCheck(this), dateCheck(this);">
					<div style="font-size: 18px">
						NEW LEAVE REQUEST FORM<br /> <br />
					</div>
					<div class="demo">
						<label for="from">From</label> <input type="text" id="from"
							name="leave_from" /> <label for="to">to</label> <input
							type="text" id="to" name="leave_to" />
					</div>
					<div>
						Leave Type: <select name="leave_type">
							<option selected="selected">casual</option>
							<option>sick</option>
						</select> <br /> <br /> Leave Reason:<input type="text"
							name="leave_reason" size="75">
						<button type="submit" value="submit">Submit</button>
						<button type="reset" value="reset">Reset</button>
					</div>
				</form>
			</div>
		</div>
	</center>
</body>
</html>
