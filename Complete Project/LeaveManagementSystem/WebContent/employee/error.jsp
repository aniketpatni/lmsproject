<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
	if (session.getAttribute("message") != null) {
		session.removeAttribute("message");
	}
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Page Not Exist - LMS</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}else {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
			document.getElementById('emp1').style.display = "none";
			document.getElementById('emp2').style.display = "none";
		}
	}
</script>
<link rel="stylesheet" href="http://localhost:8080/LeaveManagementSystem/style/styles.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="http://localhost:8080/LeaveManagementSystem/images/snapdeal_logo_tagline.png" style="float: left;" />
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
			Leave Management System <%out.print(request.getAttribute("navigation")); %>
			</div><br />
			<div>
				<ul>
					<li id="emp1"><a href="http://localhost:8080/LeaveManagementSystem/employee/createleave.jsp">New Leave Request</a></li>
					<li id="emp2"><a href="http://localhost:8080/LeaveManagementSystem/employee/showhistory">Show Own Leave History</a></li>
					<li id="manager1"><a href="http://localhost:8080/LeaveManagementSystem/manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="http://localhost:8080/LeaveManagementSystem/manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="http://localhost:8080/LeaveManagementSystem/hr/admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2"><a href="/hr/employeetasks/employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>Page you are trying to reach, does not exist, please navigate accordingly</div>
		</div>
	</center>
</body>
</html>