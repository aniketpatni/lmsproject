<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Leave Management System - LMS</title>
<link rel="stylesheet" href="../style/styles.css">

<script type="text/javascript">
	function checkType() {

		var empId = document.getElementById("EmployeeId").value;

		var i = empId.length;

		if (EmployeeId.value == "") {

			alert('Enter Employee Id');
			return false;
		} else {
			if (Password.value == "") {
				alert('Enter Password');
				return false;
			} else {

				for (i = 0; i < empId.length; i++) {

					if ((empId.charAt(i) < '0') || (empId.charAt(i) > '9')) {
						alert('Enter Valid Employee Id');
						return false;
					}
				}

				return true;
			}
		}

	}
</script>



</head>
<body style="display: block">
	<center>
		<div style="width: 1200px; float: center">
			<div style="background-color: #F8F8F8; height: 85px">
				<img src="./images/snapdeal_logo_tagline.png"
					style="float: left; position: relative; margin-top: 20px" />
				<div
					style="color: #3366FF; font-size: 35px; position: relative; top: 20px; left: 20px;">Leave
					Management System</div>
			</div>
			<br> <br>
			<div>


				<div
					style="float: right; margin-top: 10px; background-color: #E6E6FA; width: 300px; height: 420px">
					<h3>
						<a href="#"
							style="text-decoration: none; margin-left: 30px; margin-bottom: 20px">Login</a>
					</h3>
					<div style="margin-left: 30px" align="left">

						<div id="invalid" style="visibility: hidden"></div>
						<br>
						<%
							if (session.getAttribute("messages") != null) {

								String msg = session.getAttribute("messages").toString();
								out.print(msg);
							}
							session.removeAttribute("messages");
						%>


						<form method="post" action="./login" onsubmit="return checkType()">
							<br>
							<div>
								<label> <strong> Employee Id </strong>
								</label>
							</div>
							<div>
								<input type="text" name="employee_id" id="EmployeeId" size="20"
									style="position: relative; top: 10px" />
							</div>
							<br>
							<div>
								<label> <strong> Password </strong>
								</label>
							</div>
							<div>
								<input type="password" name="password" id="Password" size="20"
									style="position: relative; top: 10px" />
							</div>
							<br>

							<%
								boolean attr = session.isNew();

								if (attr == false && session.getAttribute("error") != null) {

									String msg = session.getAttribute("error").toString();
									out.print(msg);
								}
							%>

							<div>
								<button type="submit"
									style="background-color: #0099FF; border-radius: 8px; color: white; width: 80px; height: 30px; font-size: 15px; position: relative; top: 20px">login</button>
							</div>
							<br>
							<div style="position: relative; top: 20px">
								<a href="./forgotPassword.jsp">Forgot Password</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</center>
</body>
</html>
