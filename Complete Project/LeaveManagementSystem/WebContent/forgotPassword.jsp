<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Forgot Password - LMS</title>
<style type="text/css">
div.show {
	visibility: hidden
}
</style>

<script type="text/javascript">
	function checkType() {

		var empId = document.getElementById("EmployeeId").value;
		var i = empId.length;
		for (i = 0; i < empId.length; i++) {
			if ((empId.charAt(i) < '0') || (empId.charAt(i) > '9')) {
				alert('Enter Valid Employee Id');
				return false;
			}
		}
		return true;

	}
</script>
<link rel="stylesheet" href="../style/styles.css">
</head>
<body style="display: block">
	<center>
		<div style="background-color: #F8F8F8; height: 85px; width: 1200px;">
			<img src="./images/snapdeal_logo_tagline.png"
				style="float: left; position: relative; margin-top: 20px" />
			<div
				style="color: #3366FF; font-size: 35px; position: relative; top: 20px;">Leave
				Management System</div>
		</div>
		<br> <br>
		<div style="text-align: left; width: 1200px;">
			Leave Management System <%out.print(request.getAttribute("navigation")); %>
			</div><br />

		<div style="width: 1200px;">

			<div
				style="float: right; margin-top: 10px; background-color: #E6E6FA; width: 300px; height: 320px">
				<h3>
					<a href="#"
						style="text-decoration: none; margin-left: 10px; margin-bottom: 0px;">Enter
						Employee Id</a>
				</h3>
				<div style="margin-left: 20px" align="left">
					<br>
					<form method="post" action="./forgotpassword"
						onsubmit="return checkType()">

						<div>
							<label> <strong> Employee Id </strong>
							</label>
						</div>
						<div>
							<input type="text" name="emp_id" id="EmployeeId" size="15"
								style="position: relative; top: 10px" />
						</div>
						<br>
						<%
							if (session.getAttribute("messages") != null) {
								String msg = session.getAttribute("messages").toString();
								out.print(msg);
							}
							session.removeAttribute("messages");
						%>
						<div>
							<button type="submit"
								style="background-color: #0099FF; border-radius: 8px; color: white; width: 80px; height: 30px; font-size: 15px; position: relative; top: 20px">Submit</button>
						</div>
						<br>
					</form>
				</div>
			</div>
		</div>
	</center>
</body>
</html>