<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Show Team Leave Request History - LMS</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../style/styles.css">
	<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
			<<div style="float: right; font-size: 14px">
				<a href="../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
			Leave Management System > Manager > View Requests History
<%-- 			<%out.print(request.getAttribute("navigation")); %> --%>
			</div><br />
			<div>
				<ul>
					<li><a href="../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="./ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li class="active" id="manager2"><a
						href="./ViewRequestHistory">Show Employees Leave History</a></li>
					<li id="hr1"><a href="../hr/admintasks/admintask.html">HR Admin Tasks</a></li>
					<li id="hr2"><a href="../hr/employeetasks/employeetask.html">HR Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <span>
				<h1 align="center">Processed Leave Request History of Team</h1>
			</span>
			<div>
				<table>
					<thead>
						<tr>
							<th width="131" height="76" scope="col">Employee Id</th>
							<th width="155" scope="col">Employee Name</th>
							<th width="130" scope="col">Leaves From</th>
							<th width="112" scope="col">Leaves To</th>
							<th width="119" scope="col">Applied On</th>
							<th width="133" scope="col">Leave Status</th>
							<th width="137" scope="col">Leaves Type</th>
							<th width="151" scope="col">Leaves Reason</th>
							<th width="167" scope="col">No Of Leave Days</th>
						</tr>
					</thead>
					<c:forEach items="${leaveRequestList}" var="i"
						varStatus="loopStatus">
						<tr>
							<td height="99"><c:out value="${i.emp.empId}" /></td>
							<td><c:out value="${i.emp.name}" /></td>
							<td><c:out value="${i.leaveFrom}" /></td>
							<td><c:out value="${i.leaveTo}" /></td>
							<td><c:out value="${i.appliedOn}" /></td>
							<td><c:out value="${i.state.state}" /></td>
							<td><c:out value="${i.leaveType.leaveType}" /></td>
							<td><c:out value="${i.leaveReason}" /></td>
							<td><c:out value="${i.noOfLeaveDays}" /></td>
						</tr>
					</c:forEach>
				</table>
				<p>&nbsp;</p>
			</div>
		</div>
	</center>
</body>
</html>