<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet" href="../../themes/base/jquery.ui.all.css">
<script src="../../jquery-1.7.1.js"></script>
<script src="../../ui/jquery.ui.core.js"></script>
<script src="../../ui/jquery.ui.widget.js"></script>
<script src="../../ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" href="../demos.css">
<script>
	$(function() {
		$("#datepicker").datepicker({
			showButtonPanel : true
		});
	});
</script>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">
<!-- <script>
	function validateForm() {

		var empId = document.getElementById("empId").value;

		var i = empId.length;

		;
		if (i == 0) {
			alert('Enter Employee Id');
			return false;
		}
		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < '0') || (empId.charAt(i) > '9')) {
				alert('Enter Valid Employee Id');
				return false;
			}
		}

		empId = document.getElementById("empName").value;

		i = empId.length;
		alert(i);
		if (i == 0) {
			alert('Enter Employee Name');
			return false;
		}
		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < 'a') || (empId.charAt(i) > 'z')
					|| (empId.charAt(i) < 'A') || (empId.charAt(i) < 'Z')) {
				alert('Enter Valid Name');
				return false;
			}
		}

		empId = document.getElementById("designation").value;

		i = empId.length;

		if (i == 0) {
			alert('Enter Designation');
			return false;
		}
		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < 'a') || (empId.charAt(i) > 'z')	) {
				alert('Enter Valid Designation');
				return false;
			}
		}

		empId = document.getElementById("empDept").value;

		i = empId.length;

		if (i == 0) {
			alert('Enter Employee Department');
			return false;
		}

		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < 'a') || (empId.charAt(i) > 'z')
					|| (empId.charAt(i) < 'A') || (empId.charAt(i) < 'Z')) {
				alert('Enter Valid Employee Department');
				return false;
			}
		}

		empId = document.getElementById("sickLeave").value;

		i = empId.length;

		if (i == 0) {
			alert('Enter No of Sick Leaves');
			return false;
		}

		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < '0') || (empId.charAt(i) > '9')) {
				alert('Enter Valid sick leaves ');
				return false;
			}
		}

		empId = document.getElementById("status").value;

		i = empId.length;

		if (i == 0) {
			alert('Enter Status');
			return false;
		}

		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < 'a') || (empId.charAt(i) > 'z')
					|| (empId.charAt(i) < 'A') || (empId.charAt(i) < 'Z')) {
				alert('Enter Valid Status');
				return false;
			}
		}

		empId = document.getElementById("casualLeave").value;

		i = empId.length;

		if (i == 0) {
			alert('Enter No of Casual Leaves');
			return false;
		}

		for (i = 0; i < empId.length; i++) {

			if ((empId.charAt(i) < '0') || (empId.charAt(i) > '9')) {
				alert('Enter Valid casual leaves ');
				return false;
			}
		}

		return true;
	}
</script> -->

</head>
<body>
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
                       Leave Management System > HR > Employee Tasks  > Add Employee
<%--                        <%out.print(request.getAttribute("navigation")); %> --%>
            </div><br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2" class="active"><a href="./employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<br />
	<h2> Add an employee to the roster </h2>		
	<div style="margin-top: 10px; font-size: 150%;">
		<form action="AddEmployee" method="post" > <!-- onsubmit="return validateForm()" --> 
			<table cellspacing="2" cellpadding="1" style="width:50%; align:left;">
				<tbody>

					<tr>
						<td><label> Name:</label></td>
						<td>
							<div>
								<input type="text" id="empName" name="empName" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Employee Id</label></td>
						<td>
							<div>
								<input type="text" id="empId" name="empId" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Employee Department</label></td>
						<td>
							<div>
								<input type="text" id="empDept" name="empDept" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Enter Email:</label></td>
						<td>
							<div>
								<input type="text" id="empEmail" name="empEmail" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Manager Id:</label></td>
						<td>
							<div>
								<input type="text" id="managerId" name="managerId" value="" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Date of Joining:</label></td>
						<td>
							<div>
								<input type="text" id="datepicker" name="joiningDate" value="" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label>Designation:</label></td>
						<td>
							<div>
								<select name="designation" value="" style="width:66%;">
										<option>Select one</option>
										<option>tech_hr</option>
										<option>engineering_manager</option>
										<option>software_engineer</option>
										<option>product_analyst</option>
										<option>product_analyst_manager</option>
										<option>product_hr</option>
								</select>
							</div>
						</td>
					</tr>

					<tr>
						<td><label>No. of Sick Leaves:</label></td>
						<td>
							<div>
								<input type="text" id="sickLeaves" name="sickLeaves" value="" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label> No. of Casual Leaves:</label></td>
						<td>
							<div>
								<input type="text" id="casualLeaves" name="casualLeaves" value="" />
							</div>
						</td>
					</tr>

					<tr>
						<td><label> Status:</label></td>
						<td><label>
								<select name="status" style="width:66%;">
										<option>Select one</option>
										<option>On Roll</option>
										<option>On Leave</option>
										<option>On Probation</option>
										<option>Serving Notice Period</option>
										<option>Resigned</option>
								</select>
							</label>
						</td>
					</tr>
			</table>
			<br> <input type="submit" value="Submit" />
		</form>
	</div>
	</div>
	</center>
</body>
</html>
