<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Viewing Leave History For Employee Id ${empId}</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
                       Leave Management System > HR > Employee Tasks  > Detailed Leave History
<%--                        <%out.print(request.getAttribute("navigation")); %> --%>
            </div><br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2" class="active"><a href="./employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>
				<table>
					<tr>
						<th>Leave Id</th>
						<th>Employee Id</th>
						<th>Employee Name</th>
						<th>Leave Message</th>
						<th>Previous State</th>
						<th>Current State</th>
						<th>Time of change</th>
						<th>Logger Id</th>
						<th>Logger Name</th>
					</tr>
					<c:forEach var="i" items="${ListOfLogs}" varStatus="loopStatus">
						<tr>
							<td><c:out value="${i.leave.leaveId}" /></td>
							<td><c:out value="${i.emp.empId}" /></td>
							<td><c:out value="${i.emp.name}" /></td>
							<td><c:out value="${i.leaveMsg}" /></td>
							<td><c:out value="${i.prevState.state}" /></td>
							<td><c:out value="${i.currState.state}" /></td>
							<td><c:out value="${i.timeOfChange}" /></td>
							<td><c:out value="${i.loggerEmp.empId}" /></td>
							<td><c:out value="${i.loggerEmp.name}" /></td>
						</tr>
					</c:forEach>
				</table>
				<div>
					<a
						href="OverallLeaveHistory?empId=${param['empId']}">Click
						here to see employee's leave history</a>
				</div>
				<br />
			</div>
		</div>
	</center>
</body>
</html>
