<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Employee Status</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">

</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
                       Leave Management System > HR > Employee Tasks  > Employee Status
<%--                        <%out.print(request.getAttribute("navigation")); %> --%>
            </div><br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../admintasks/admintask.html">HR
							Admin Tasks</a></li>
					<li id="hr2" class="active"><a href="./employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>

				<div>
					<h2>Please Update Status </h2><br/>
					<form action="ChangeEmpStatus" method="post">
						<div style="margin-left:40%;float:left;width:20%;padding-left:10;text-align:left">Employee Id :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeDetails.empId}</div><br/>
						<div style="clear:both"></div>
						<div style="margin-left:40%;float:left;width:20%;padding-left: 10;text-align: left;">Employee name :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeDetails.name}</div><br/>
						<div style="clear:both"></div>
						<div style="margin-left:40%;float:left;width:20%;padding-left: 10;text-align: left;">Current Status :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeDetails.empStatus.empStatus}</div><br/><br/><br/><br/>
						Update Status: <select name="empStatus">
										<option>Select one</option>
										<option>On Roll</option>
										<option>On Leave</option>
										<option>On Probation</option>
										<option>Serving Notice Period</option>
										<option>Resigned</option>
									</select>
						
						<button type="submit">Update</button>
					</form>
				</div>
				<br />
				<div>
					<a href="SearchUser?empId=${param['empId']}"><button >Click here to go back</button></a>
				</div>
				<br />
			</div>
		</div>
	</center>
</body>
</html>
