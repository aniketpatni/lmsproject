<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta content="utf-8">
<title>HR Leave Management</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../themes/base/jquery.ui.all.css">
<script src="../../js/jquery-1.7.1.js"></script>
<script src="../../js/jquery.ui.core.js"></script>
<script src="../../js/jquery.ui.widget.js"></script>
<script src="../../js/jquery.ui.datepicker.js"></script>
<!-- <script src="../../js/jquery.ui.accordion.js"></script> -->
<script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-ui-1.8.13.custom.min.js"></script>
<script type="text/javascript" src="../../js/jquery.multi-accordion-1.5.3.js"></script>
<link rel="stylesheet" href="../../style/demos.css">
<script type="text/javascript">
						$(function() {
							$('#multiAccordion').multiAccordion({
								click : function(event, ui) {
									//console.log('clicked');
								},
								init : function(event, ui) {
									//console.log('whoooooha');
								},
								tabShown : function(event, ui) {
									//console.log('shown');
								},
								tabHidden : function(event, ui) {
									//console.log('hidden');
								}

							});

							$('#multiAccordion').multiAccordion("option",
									"active", [ 0 ]);
						});
					</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table_only.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div style="text-align: left;">
                       Leave Management System > HR > Admin tasks > Holiday Management
<%--                        <%out.print(request.getAttribute("navigation")); %> --%>
            </div><br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="./admintask.html">HR Admin Tasks</a></li>
					<li id="hr2"><a href="../employeetasks/employeetask.html">HR
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>
				<div class="demo4">

					<div id="multiAccordion">

						<h3>
							<a href="#">List of All Official Holidays </a>
						</h3>
						<div>
							<img src="../../images/white-bg.gif" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0.45; z-index: -1;"/>
							<table class="table-class" style="width: 50%; border: 1px solid #000000; text-align: center;">
								<tr>
									<th>Date</th>
									<th>Day</th>
									<th>Holiday</th>
								</tr>
								<c:forEach var="i" items="${listOfHolidays}"
									varStatus="loopStatus">
									<tr>
										<td><fmt:formatDate value="${i.date}" pattern="yyyy/MM/dd"></fmt:formatDate></td>
										<td><c:choose>
												<c:when test="${i.date.day == 0}">Sunday</c:when>
												<c:when test="${i.date.day == 1}">Monday</c:when>
												<c:when test="${i.date.day == 2}">Tuesday</c:when>
												<c:when test="${i.date.day == 3}">Wednesday</c:when>
												<c:when test="${i.date.day == 4}">Thursday</c:when>
												<c:when test="${i.date.day == 5}">Friday</c:when>
												<c:when test="${i.date.day == 6}">Saturday</c:when>
											</c:choose></td>
										<td><c:out value="${i.holiday}" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>

						<h3>
							<a href="#">Add a holiday </a>
						</h3>
						<div>
							<img src="../../images/white-bg.gif" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0.45; z-index: -1;"/>
							<form action="./AddHolidaysOneByOne" method="post">
								<script>
									$(function() {
										$("#datepicker").datepicker();
									});
									$(function() {
										$("#datepicker").datepicker("option", "dateFormat", "yy/mm/dd");
									});
								</script>
								<div style="font-size:85%; width:48%; float: left;">
										<span>ADD HOLIDAYS ONE BY ONE</span><br/><br/>
										<div style="width:50%;float:left;">Date: </div><input type="text" name="date" id="datepicker"> <br/>
										<div style="width:50%;float:left;">Name of the Holiday:</div><input type="text" name="holiday" /><br/>
										<div style="clear:both"></div><br/>
										<button type="submit" value="Submit">Add Holiday</button>
								</div>
							</form>
							<div style="font-size: 180%; width:4%; float: left; margin-top: 3%">
									OR
							</div>
							<form action="./AddHolidayByCSV" method="post">
								<div style="font-size:85%; width:48%; float: left;">
									<div>
										ADD CSV FILE WITH HOLIDAYS<br /> <br/>
										<input type="file" accept="text/csv">
									</div> <br/><br/>
										<button type="submit" value="Submit">Add Holiday</button>
								</div>
								<div style="clear: both;"></div>
							</form>
						</div>

						<h3>
							<a href="#">Delete a Holiday </a>
						</h3>
						<div style="padding-top: 2%">
							<img src="../../images/white-bg.gif" style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0.45; z-index: -1;"/>
							<form id="frm4" action="./DeleteHolidayByDate" method="get">

								<script>
									$(function() {
										$("#datepicker1").datepicker();
										$("#datepicker1").datepicker("option", "dateFormat", "yy/mm/dd");
									});
								</script>
								
								<div>
										Date: <input type="text" name="date" id="datepicker1">&nbsp;&nbsp;
										Name of the Holiday : <input type="text" name="holiday" /><span style="font-size: 70%">(Optional for unique dates)</span><br/><br/>
										<button type="submit" value="Submit">Delete Holiday</button>
								</div>
								
							</form>
						</div>
					</div>

					

				</div>
				<br />
				<div>
					<a href="./admintask.html"> Click here to go back</a>
				</div>
			</div>
		</div>
	</center>
</body>
</html>
