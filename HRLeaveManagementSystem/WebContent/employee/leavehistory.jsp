
<%@page import="snapdeal.lms.entityDAO.LeaveRequest"%>
<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Employee Leave History - LMS</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../style/styles.css">
<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<div style="width: 1200px;" align="center">
		<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
		<div style="float: right; font-size: 14px">
			<a href="../logout">logout</a>
		</div>
		<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
			Management System</p>
		<br />
		<br />
		<br />
		<br />
		<div>
			<ul>
				<li><a href="./createleave.jsp">New Leave Request</a></li>
				<li class="active"><a href="./showhistory">Show Own Leave
						History</a></li>
				<li id="manager1"><a href="../manager/ViewActiveRequest">Manage
						Pending Leave Requests</a></li>
				<li id="manager2"><a href="../manager/ViewRequestHistory">Show
						Employees Leave History</a></li>
				<li id="hr1"><a href="../hr/admin.jsp">Hr Admin Tasks</a></li>
				<li id="hr2"><a href="../hr/employee.jsp">Hr Employees
						Tasks</a></li>
			</ul>
		</div>
		<br /> <br /> <br /> <br /> <br />
		<div id="empDetails">
			<p>
				Employee ID:
				<c:out value="${emp.empId}"></c:out>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Employee Name:
				<c:out value="${emp.name}"></c:out>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Current
				Casual Leave Balance:
				<c:out value="${emp.casualLeaveBalance}"></c:out>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Current
				Sick Leave Balance:
				<c:out value="${emp.sickLeaveBalance}"></c:out>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</p>
		</div>
		<br /> <br /> <br />
		<div>
			<table width=100% align="center" cellpadding="5">
				<thead>
					<tr>
						<td width="55"><div align="center">Leave ID</div></td>
						<td width="60"><div align="center">Leave From</div></td>
						<td width="60"><div align="center">Leave To</div></td>
						<td width="200"><div align="center">Request Applied on</div></td>
						<td width="200"><div align="center">State</div></td>
						<td width="70"><div align="center">Leave Type</div></td>
						<td width="295"><div align="center">Leave Reason</div></td>
						<td width="50">
							<div align="center">No. of Leave Days</div>
						</td>
						<td width="200"><div align="center">Update/Delete Leave
								Request</div></td>
					</tr>
				</thead>
				<c:forEach var="i" items="${leave}" varStatus="loopStatus">
					<tr>
						<td>
							<form name="leaveform" action="./viewlog" method="post"
								class="leaveform">
								<input type='text' name='leave_id' value="${i.leaveId}"
									style="display: none;" />
								<button type="submit">
									<c:out value="${i.leaveId}" />
								</button>
							</form>
						</td>
						<td><c:out value="${i.leaveFrom}" /></td>
						<td><c:out value="${i.leaveTo}" /></td>
						<td><c:out value="${i.appliedOn}" /></td>
						<td><c:out value="${i.state.state}" /></td>
						<td><c:out value="${i.leaveType.leaveType}" /></td>
						<td><c:out value="${i.leaveReason}" /></td>
						<td><c:out value="${i.noOfLeaveDays}" /></td>
						<td><div id="${i.leaveId}">
								<script type="text/javascript">
									checkDisplay();
								</script>
								<script type="text/javascript">
									function checkDisplay() {
										var leavestate = '${i.state.state}';
										if (leavestate == "pending_approval_by_manager") {
											document.getElementById('${i.leaveId}').style.display = "inline";
										} else {
											document.getElementById('${i.leaveId}').style.display = "none";
										}
									}
								</script>
								<form name="deleteform" action="./deleteleave" method="post"
									class="deleteform">
									<input type='text' name='leave_id' value="${i.leaveId}"
										style="display: none;" />
									<button type="submit" style="float: right;">Reject</button>
								</form>
								<form name="updateform" action="./updateleave" method="post"
									class="updateform">
									<input type='text' name='leave_id' value="${i.leaveId}"
										style="display: none;" />
									<button type="submit" style="float: right;">Update</button>
								</form>
							</div></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>