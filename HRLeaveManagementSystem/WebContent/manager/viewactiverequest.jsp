<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script type="text/javascript">
	function submitform() {
		document.getElementById('acceptform').submit();
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Pending Requests - LMS</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../style/styles.css">
	<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<div style="width: 1200px;" align="center">
		<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
		<div style="float: right; font-size: 14px">
			<a href="../logout">logout</a>
		</div>
		<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
			Management System</p>
		<br />
		<br />
		<br />
		<br />
		<div>
			<ul>
				<li><a href="../employee/createleave.jsp">New Leave Request</a></li>
				<li><a href="../employee/showhistory">Show Own Leave
						History</a></li>
				<li class="active" id="manager1"><a href="./ViewActiveRequest">Manage
						Pending Leave Requests</a></li>
				<li id="manager2"><a href="./ViewRequestHistory">Show
						Employees Leave History</a></li>
				<li id="hr1"><a href="../hr/admin.jsp">Hr Admin Tasks</a></li>
				<li id="hr2"><a href="../hr/employee.jsp">Hr Employees
						Tasks</a></li>
			</ul>
		</div>
		<br /> <br /> <br /> <br /> <br />
		<h2 align="center">
			<br /> <span>Pending Leave Request Information of the Team </span><span></span><span></span>
		</h2>
		<div>
			<table>
				<thead>
					<tr>
						<th width="136" height="57" scope="col">Employee Id</th>
						<th width="158" scope="col">Employee Name</th>
						<th width="155" scope="col">Leaves From</th>
						<th width="155" scope="col">Leaves To</th>
						<th width="178" scope="col">Applied On</th>
						<th width="205" scope="col">Accept/Reject</th>
					</tr>
				</thead>
				<c:forEach items="${leaveRequestList}" var="i"
					varStatus="loopStatus">
					<tr>
						<td height="103"><c:out value="${i.emp.empId}" /></td>
						<td><c:out value="${i.emp.name}" /></td>
						<td><c:out value="${i.leaveFrom}" /></td>
						<td><c:out value="${i.leaveTo}" /></td>
						<td><c:out value="${i.appliedOn}" /></td>
						<td width="205"><form name="acceptform" id="acceptform"
								action="AcceptRequest" method="post">
								<input type="hidden" name="leaveId" value="${i.leaveId}" />
								<div align="center">
									<a href="javascript:submitform()"><u>View Request</u></a>

								</div>
							</form></td>

					</tr>
				</c:forEach>
			</table>
			<p>&nbsp;</p>
		</div>
	</div>
</body>
</html>