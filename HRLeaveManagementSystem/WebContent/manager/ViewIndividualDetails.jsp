<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Individual Leave Details</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../style/styles.css">
	<link rel="stylesheet" href="../style/table.css">
</head>
<body onload="checkRole()">
	<div style="width: 1200px;" align="center">
		<img src="../images/snapdeal_logo_tagline.png" style="float: left;" />
		<div style="float: right; font-size: 14px">
			<a href="../logout">logout</a>
		</div>
		<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
			Management System</p>
		<br />
		<br />
		<br />
		<br />
		<div>
			<ul>
				<li><a href="../employee/createleave.jsp">New Leave Request</a></li>
				<li><a href="../employee/showhistory">Show Own Leave
						History</a></li>
				<li id="manager1"><a href="./ViewActiveRequest">Manage
						Pending Leave Requests</a></li>
				<li id="manager2"><a href="./ViewRequestHistory">Show
						Employees Leave History</a></li>
				<li id="hr1"><a href="../hr/admin.jsp">Hr Admin Tasks</a></li>
				<li id="hr2"><a href="../hr/employee.jsp">Hr Employees
						Tasks</a></li>
			</ul>
		</div>
		<br />
		<br />
		<br />
		<br />
		<div>
			<div>
				<h2 align="center">Details of Employee Taking Request</h2>
			</div>
			<div>
				<table>
					<thead>
						<tr>
							<th width="59" scope="col">Employee ID</th>
							<th width="59" scope="col">Employee Name</th>
							<th width="118" scope="col">Employee Department</th>
							<th width="118" scope="col">Employee EmailID</th>
							<th width="118" scope="col">Date of Joining</th>
							<th width="118" scope="col">Designation</th>
							<th width="118" scope="col">Sick Leave Balance</th>
							<th width="118" scope="col">Casual Leave Balance</th>
						</tr>
					</thead>
					<tr>
						<td>${individualRequest.emp.empId}</td>
						<td>${individualRequest.emp.name}</td>
						<td>${individualRequest.emp.dept}</td>
						<td>${individualRequest.emp.emailId}</td>
						<td>${individualRequest.emp.dateOfJoining}</td>
						<td>${individualRequest.emp.designation.designation}</td>
						<td>${individualRequest.emp.sickLeaveBalance}</td>
						<td>${individualRequest.emp.casualLeaveBalance}</td>
					</tr>
				</table>
			</div>
			<br />
			<br />
			<div>
				<h2 align="center">Details of the Leave Requested By Employee</h2>
			</div>
			<div>
				<table>
					<tr>
						<th width="59" scope="col">Leave Start Date</th>
						<th width="59" scope="col">Leave End Date</th>
						<th width="118" scope="col">Leave Application Date</th>
						<th width="118" scope="col">Leave Type</th>
						<th width="118" scope="col">Leave Reason</th>
						<th width="118" scope="col">No Of Leaves</th>
					</tr>
					<tr>

						<td>${individualRequest.leaveFrom}</td>
						<td>${individualRequest.leaveTo}</td>
						<td>${individualRequest.appliedOn}</td>
						<td>${individualRequest.leaveType.leaveType}</td>
						<td>${individualRequest.leaveReason}</td>
						<td>${individualRequest.noOfLeaveDays}</td>
					</tr>

				</table>

				<div>
					<form name="myform" action="ActiveRequestAction">
						<div>
							<input type="hidden" name="leaveId"
								value="${individualRequest.leaveId}" />
							<!-- <input	type="radio" name="action" value="approved_by_manager" checked />Accept<br/> -->
							<!-- <input type="radio" name="action" value="rejected_by_manager" />Reject </p> -->
							<div>
								<br /> Comments:
							</div>
							<div>
								<textarea name="remarks" cols="75" rows="4"></textarea>
							</div>
							<div align="center">
								<br />
								<div class="demo">
									<input type="submit" value="accept" name="action" /> <input
										type="submit" value="reject" name="action" />
								</div>
							</div>

						</div>
					</form>
				</div>

			</div>
		</div>
	</div>
</body>
</html>