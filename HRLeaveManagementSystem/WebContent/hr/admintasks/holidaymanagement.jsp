<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta content="utf-8">
<title>HR Leave Management</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {
			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {
			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../js/jquery.ui.all.css">
<script src="../../js/jquery-1.7.1.js"></script>
<script src="../../js/jquery.ui.core.js"></script>
<script src="../../js/jquery.ui.widget.js"></script>
<script src="../../js/jquery.ui.datepicker.js"></script>
<script src="../../js/jquery.ui.accordion.js"></script>
<script type="text/javascript" src="../../js/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="../../js/jquery-ui-1.8.13.custom.min.js"></script>
<script type="text/javascript" src="../../js/jquery.multi-accordion-1.5.3.js"></script>
<link rel="stylesheet" href="../../css/demos.css">
<script>
	$(function() {
		$("#accordion").accordion();
	});
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">
</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="./admintask.html">Hr Admin Tasks</a></li>
					<li id="hr2"><a href="../employeetasks/employeetask.html">Hr
							Employees Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>
				<div class="demo4">
					<script>
						function formSubmit() {
							document.getElementById("frm5").submit();
						}
					</script>

					<div id="multiAccordion">

						<h3>
							<a href="#">List of All Official Holidays </a>
						</h3>
						<div>
							<center>
								<table>
									<c:forEach var="i" items="${listOfHolidays}"
										varStatus="loopStatus">
										<tr>
											<td><c:out value="${i.date}" /></td>
											<td><c:out value="${i.holiday}" /></td>
										</tr>
									</c:forEach>
								</table>
							</center>
						</div>
						<h3>
							<a href="#">Add a holiday </a>
						</h3>
						<div>

							<form action="./AddHolidaysOneByOne" method="post">
								<script>
									$(function() {
										$("#datepicker").datepicker();
									});
									$(function() {
										$("#datepicker").datepicker("option",
												"dateFormat", "yy/mm/dd");
									});
								</script>
								<div style="font-size: 120%; width =50%; float: left;">
									<div>

										Date: <input type="text" name="date" id="datepicker">
										<span>Name of the Holiday</span> <span><input
											type="text" name="holiday" /></span>
									</div>
									<div>
										<button type="submit" value="Submit">Add Holiday</button>

									</div>
								</div>
							</form>

							<form action="./AddHolidayByCSV" method="post">
								<div style="font-size: 120%; width =50%; float: left;">
									<div class="demo">

										Add csv file with holidays<br /> <input type="file"
											accept="text/csv">
									</div>
									<div>
										<button type="submit" value="Submit">Add Holiday</button>

									</div>
								</div>
								<div style="clear: both;"></div>
							</form>
						</div>




						<h3>
							<a href="#">Delete a Holiday </a>
						</h3>
						<div>
							<script>
								function formSubmit() {
									document.getElementById("frm4").submit();
								}
							</script>

							<form id="frm4" action="./DeleteHolidayByDate" method="get">

								<script>
									$(function() {
										$("#datepicker1").datepicker();
									});
									$(function() {
										$("#datepicker1").datepicker("option",
												"dateFormat", "yy/mm/dd");
									});
								</script>

								<div style="font-size: 120%">
									<div>

										Date: <input type="text" name="date" id="datepicker1">
										<span>Name of the Holiday</span> <span><input
											type="text" name="holiday" /></span>
									</div>
									<div>
										<button type="submit" value="Submit">Delete Holiday</button>

									</div>
								</div>
							</form>
						</div>

					</div>

					<script type="text/javascript">
						$(function() {
							$('#multiAccordion').multiAccordion({
								active : [ 1, 2 ],
								click : function(event, ui) {
									//console.log('clicked')
								},
								init : function(event, ui) {
									//console.log('whoooooha')
								},
								tabShown : function(event, ui) {
									//console.log('shown')
								},
								tabHidden : function(event, ui) {
									//console.log('hidden')
								}

							});

							$('#multiAccordion').multiAccordion("option",
									"active", [ 0, 3 ]);
						});
					</script>

				</div>
				<br />
				<div>
					<a href="./admintask.html"> Click here to go back</a>
				</div>
			</div>
		</div>
	</center>
</body>
</html>
