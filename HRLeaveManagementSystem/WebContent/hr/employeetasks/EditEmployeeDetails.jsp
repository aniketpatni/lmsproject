<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Employee Details</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">

</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a
						href="../admintasks/admintask.html">Hr Admin Tasks</a></li>
					<li id="hr2" class="active"><a href="./employeetask.html">Hr Employees
							Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>
				<div>
					Edit Employee Details : <br />
					<form action="ChangeEmployeeDetails" method="post">
						Employee Id : <input name="empId" value="${employeeEdit.empId}">
						<br /> Employee name : <input name="empName"
							value="${employeeEdit.name}"> <br /> Current Status : <input
							name="status" value="${employeeEdit.empStatus.empStatus}"
							disabled="disabled"> <br /> Department: <input
							name="empDept" value="${employeeEdit.dept}"><br />
						EmailId: <input name="empEmail" value="${employeeEdit.emailId}"><br />
						Manager: <input name="managerId"
							value="${employeeEdit.manager.empId}"><br /> Designation:<input
							name="designation"
							value="${employeeEdit.designation.designation}"><br />
						Sick Leave Balance:<input name="sickLeaves"
							value="${employeeEdit.sickLeaveBalance}"><br /> Casual
						Leave Balance:<input name="casualLeaves"
							value="${employeeEdit.casualLeaveBalance}"><br /> The
						employee to hand over:<input name="handOverToEmp"
							value="${employeeEdit.handOverToEmp.empId}"><br />
						<button type="submit">Update</button>
					</form>
				</div>
				<br />
				<div>
					<a href="SearchUser?empId=${param['empId']}">Click here to go
						back</a>
				</div>
				<br />
			</div>
		</div>
	</center>
</body>
</html>
