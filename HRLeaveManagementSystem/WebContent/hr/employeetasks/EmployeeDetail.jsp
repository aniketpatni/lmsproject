<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Employee Details for ${employeeObject.name}</title>
<script type="text/javascript">
	function checkRole() {
		var role1 = '${role}';
		if (role1 == "manager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "employee") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "none";
			document.getElementById('hr2').style.display = "none";
		} else if (role1 == "hr") {

			document.getElementById('manager1').style.display = "none";
			document.getElementById('manager2').style.display = "none";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		} else if (role1 == "hrmanager") {

			document.getElementById('manager1').style.display = "inline";
			document.getElementById('manager2').style.display = "inline";
			document.getElementById('hr1').style.display = "inline";
			document.getElementById('hr2').style.display = "inline";
		}
	}
</script>
<link rel="stylesheet" href="../../style/styles.css">
<link rel="stylesheet" href="../../style/table.css">

</head>
<body onload="checkRole()">
	<center>
		<div style="width: 1200px;" align="center">
			<img src="../../images/snapdeal_logo_tagline.png"
				style="float: left;" />
			<div style="float: right; font-size: 14px">
				<a href="../../logout">logout</a>
			</div>
			<div
				style="float: right; font-size: 14px; position: relative; margin-right: 15px;">
				<a href="../../update/updatePassword.jsp">Change Password</a>
			</div>
			<p style="font-family: georgia, garamond, serif; font-size: 30px;">Leave
				Management System</p>
			<br /> <br /> <br /> <br />
			<div>
				<ul>
					<li><a href="../../employee/createleave.jsp">New Leave
							Request</a></li>
					<li><a href="../../employee/showhistory">Show Own Leave
							History</a></li>
					<li id="manager1"><a href="../../manager/ViewActiveRequest">Manage
							Pending Leave Requests</a></li>
					<li id="manager2"><a href="../../manager/ViewRequestHistory">Show
							Employees Leave History</a></li>
					<li id="hr1"><a href="../admintasks/admintask.html">Hr
							Admin Tasks</a></li>
					<li id="hr2" class="active"><a href="./employeetask.html">Hr Employees
							Tasks</a></li>
				</ul>
			</div>
			<br /> <br /> <br /> <br /> <br />
			<div>
			<h3 style="margin-left:20%;float:left;width:20%;padding-left:10;text-align:left">Details of ${employeeObject.name}</h3>
			<div style="clear:both"></div><br/>
			
				<div style="margin-left:20%;float:left;width:20%;padding-left:10;text-align:left">Employee Id :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;"> ${employeeObject.empId}</div>
<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Employee Name : </div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeObject.name}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Employee Dept: </div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeObject.dept}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Employee Email id :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;"> ${employeeObject.emailId}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Relevant Manager : </div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeObject.manager.name}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Employee Designation :
					</div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeObject.designation.designation}</div>
					<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">SickLeaveBalance : </div><div style="margin-left:0%;float:left;width:30%;text-align: left;">${employeeObject.sickLeaveBalance}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Casual Leaves Balance :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;"> ${employeeObject.casualLeaveBalance}</div>
				<div style="clear:both"></div>
				<div style="margin-left:20%;float:left;width:20%;padding-left: 10;text-align: left;">Employee Status :</div><div style="margin-left:0%;float:left;width:30%;text-align: left;"> ${employeeObject.empStatus.empStatus}</div>
				<br /><br/><br />
				<br/>
				<div style="margin-left:20%;float:left;width:30%;text-align: left;">
					<a href="OverallLeaveHistory?empId=${employeeObject.empId}"><u>Click
						here to see the employee's leave history</u></a>
				</div>
				<div style="clear:both"></div>
				<br />
				<div style="margin-left:20%;float:left;width:30%;text-align: left;">
					<a href="AccessEmployeeForUpdate?empId=${employeeObject.empId}"><u>Click
						here to update employee status</u></a>
				</div>
				<div style="clear:both"></div>
				<br />
				<div style="margin-left:20%;float:left;width:30%;text-align: left;">
					<a href="AccessEmployeeDetails?empId=${employeeObject.empId}"><u>Click
						here to edit employee details</u></a>
				</div>
				<div style="clear:both"></div>
				<br /> 
				<div style="margin-left:20%;float:left;width:30%;text-align: left;">
					<a href="./employeetask.html"> <u>Click here to
						go back </u></a>
				</div>
			</div>
		</div>
	</center>
</body>
</html>
