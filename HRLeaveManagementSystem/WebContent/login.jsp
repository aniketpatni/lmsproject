<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Leave Management System</title>
<link rel="stylesheet" href="./themes/base/jquery.ui.all.css">
<script src="./js/jquery-1.7.1.js"></script>
<script src="./js/jquery.ui.core.js"></script>
<script src="./js/jquery.ui.widget.js"></script>
<script src="./js/jquery.ui.accordion.js"></script>
<link rel="stylesheet" href="./style/demos.css">
<script type="text/javascript">
	$(function() {
		$( "#accordion" ).accordion();
	});
	</script>
     <script type="text/javascript">
           function invalid(){
        	   boolean attr = false;
			   attr = session.isNew();
			   
        	if(attr == false && session.getAttribute("error") !=null)
        		{
        	        var  value =(String) session.getAttribute("error");  
           
        	 if(value.equals("invalid")){
                 document.getElementById("invalid").style.visibility = "visible";
                
               }
              if(value.equals("no access")){
            	  document.getElementById("access").style.visibility = "visible";
               };
        	};
          }
           window.onload = invalid();
        </script>        
</head>
<body onload="invalid();">
          
	<div class="demo">
		<img src="./images/snapdeal_logo_tagline.png" width=200px />
		<div id="accordion"
			style="float: right; margin-top: 50px; font-size: 150%">
			<h3>
				<a href="#" style="text-decoration: none">Employee Login</a>
			</h3>
			<div>
				
				<div id="invalid" style="visibility: hidden">InValid Employee
					Id or Password</div>
				<div id="access" style="visibility: hidden">No Authorization</div>
				
				<form method="post" action="userlogin">

					<div>Employee Id</div>
					<div>
						<input type="text" name="EmployeeID">
					</div>
					<br>
					<div>Password</div>
					<div>
						<input type="password" name="Password">
					</div>
					<br>
					<div>
						<button onclick="submit">login</button>

						<a href="./forgotPassword.jsp">Forgot
							Password</a>
					</div>
				</form>
			</div>

			<h3>
				<a href="#" style="text-decoration: none">Admin Login</a>
			</h3>
			<div>

				<% 
        //           String value =(String) session.getAttribute("");
        //           if(value.equals()){
        //              invalid();
        //            }
        //        %>

				<div id="invalid" style="visibility: hidden">InValid Employee
					Id or Password</div>
				<form method="post" action="adminlogin">
					<div>Employee Id</div>
					<div>
						<input type="text" name="employeeID">
					</div>
					<br>
					<div>Password</div>
					<div>
						<input type="password" name="password">
					</div>
					<br>
					<div>
						<button onclick="submit">login</button>
						<a href="./email.jsp">Forgot
							Password</a>
					</div>
				</form>
			</div>

		</div>

	</div>

</body>
</html>
