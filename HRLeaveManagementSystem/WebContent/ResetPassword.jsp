<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reset Password - LMS</title>

<script type="text/javascript">
   function checkPassword(){
	  
	   var pass = document.forms["form"]["pass"].value;
	    var cpass=document.forms["form"]["cpass"].value;
	   
	   if(pass != cpass){
		   alert("password doesn't match");
		   return false;
	   }
	   
   }
</script>
</head>
<body>
   
  <div>
   <form name="form" action="./changepassword" method="post" onsubmit="return checkPassword()">
   <div>
    <span>Enter New Password: </span>
    <span> <input name="pass" type="password" id="pass"/></span>
    
  </div><br>
  <div>
    <span>Confirm Password:</span>
    <span> <input name="cpass" type="password" id="cpass"/></span>
  </div><br>
  <div>
    <input type="submit" value="Submit" onclick="submit" /> 
  </div>
  </form>
  </div>
</body>
</html>