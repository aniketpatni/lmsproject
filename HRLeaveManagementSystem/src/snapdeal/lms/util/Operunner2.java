package snapdeal.lms.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import snapdeal.lms.entityDAO.DesignationLeaveMapDAO;
import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;
import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;

public class Operunner2 {
	
//	public static int getNoToBeAdded(int totalYearlyAllowed, int currQuarter){
//		int remainder = totalYearlyAllowed%4;
//		int toBeReturned = (totalYearlyAllowed-remainder)/4;
//		if(remainder==1){
//			return toBeReturned+remainder;
//		}
//		if(remainder==2){
//			if(currQuarter==1){
//				return toBeReturned+1;
//			}
//			if(currQuarter==2){
//				return toBeReturned;
//			}
//			if(currQuarter==3){
//				return toBeReturned+1;
//			}
//			if(currQuarter==4){
//				return toBeReturned;
//			}
//		}
//		if(remainder==3){
//			if(currQuarter==1){
//				return toBeReturned+1;
//			}
//			if(currQuarter==2){
//				return toBeReturned+1;
//			}
//			if(currQuarter==3){
//				return toBeReturned+1;
//			}
//			if(currQuarter==4){
//				return toBeReturned;
//			}
//		}
//		return toBeReturned;
//	}
//	
//	public static int getCurrQuarter(){
//		Calendar cal = Calendar.getInstance();
//		cal.setTimeInMillis(new java.util.Date().getTime());
//		int month = cal.get(Calendar.MONTH);
//		int quarter = (month / 3) + 1;
//		return quarter;
//	}
//	
//	public static void main(String[] args) {
//		List<Employee> empList = EmployeeDAO.getInstance().getEmployees();
//		int currQuarter = getCurrQuarter(); 
//		DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
//		for(Employee e1 : empList){
//			int totalYearlyAllowedCasual = dlmDao.getCasualLeavesByDesignation(e1.getDesignation().getDesignation());
//			int totalYearlyAllowedSick = dlmDao.getSickLeavesByDesignation(e1.getDesignation().getDesignation());
//			int toBeAddedThisQuarterCasual = getNoToBeAdded(totalYearlyAllowedCasual,currQuarter);
//			int toBeAddedThisQuarterSick = getNoToBeAdded(totalYearlyAllowedSick,currQuarter);
//			String empStatus = e1.getEmpStatus().getEmpStatus();
//			if(empStatus.equals("on_role")||empStatus.equals("on_leave")){
//				EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "casual", toBeAddedThisQuarterCasual, false);
//				EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "sick", toBeAddedThisQuarterSick, false);
//			}
//		}
//	}
	private int carryOverLeavesCasual = 0;
	private int carryOverLeavesSick = 0;
	
	public int getCarryOverLeavesCasual() {
		return carryOverLeavesCasual;
	}

	public void setCarryOverLeavesCasual(int carryOverLeavesCasual) {
		this.carryOverLeavesCasual = carryOverLeavesCasual;
	}

	public int getCarryOverLeavesSick() {
		return carryOverLeavesSick;
	}

	public void setCarryOverLeavesSick(int carryOverLeavesSick) {
		this.carryOverLeavesSick = carryOverLeavesSick;
	}

	public int removeFromCarryOverCasual(int currLeaves){
		if(currLeaves>getCarryOverLeavesCasual()){
			return currLeaves-getCarryOverLeavesCasual();
		}else{
			return 0;
		}
	}
	
	public int removeFromCarryOverSick(int currLeaves){
		if(currLeaves>getCarryOverLeavesSick()){
			return currLeaves-getCarryOverLeavesSick();
		}else{
			return 0;
		}
	}
		
	public static void main(String[] args) {
		
		List<Employee> empList = EmployeeDAO.getInstance().getEmployees();
		for(Employee e1 : empList){
			int carriedOverCasual = new Operunner2().removeFromCarryOverCasual(e1.getCasualLeaveBalance()); 
			int carriedOverSick =  new Operunner2().removeFromCarryOverSick(e1.getSickLeaveBalance()); 
			EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "casual", carriedOverCasual, true);
			EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "sick", carriedOverSick, true);
		}
		
	}
}
