package snapdeal.lms.util;

import java.util.Calendar;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import snapdeal.lms.entityDAO.DesignationLeaveMapDAO;
import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;

public class UpdateLeavesQuarterlyJob implements Job{

	public UpdateLeavesQuarterlyJob(){
	}
	
	public int getNoToBeAdded(int totalYearlyAllowed, int currQuarter){
		int remainder = totalYearlyAllowed%4;
		int toBeReturned = (totalYearlyAllowed-remainder)/4;
		if(remainder==1){
			return toBeReturned+remainder;
		}
		if(remainder==2){
			if(currQuarter==1){
				return toBeReturned+1;
			}
			if(currQuarter==2){
				return toBeReturned;
			}
			if(currQuarter==3){
				return toBeReturned+1;
			}
			if(currQuarter==4){
				return toBeReturned;
			}
		}
		if(remainder==3){
			if(currQuarter==1){
				return toBeReturned+1;
			}
			if(currQuarter==2){
				return toBeReturned+1;
			}
			if(currQuarter==3){
				return toBeReturned+1;
			}
			if(currQuarter==4){
				return toBeReturned;
			}
		}
		return toBeReturned;
	}
	
	public int getCurrQuarter(){
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(new java.util.Date().getTime());
		int month = cal.get(Calendar.MONTH);
		int quarter = (month / 3) + 1;
		return quarter;
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		
		List<Employee> empList = EmployeeDAO.getInstance().getEmployees();
		int currQuarter = getCurrQuarter(); 
		DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
		for(Employee e1 : empList){
			int totalYearlyAllowed = dlmDao.getCasualLeavesByDesignation(e1.getDesignation().getDesignation());
			int toBeAddedThisQuarter = getNoToBeAdded(totalYearlyAllowed,currQuarter);
			String empStatus = e1.getEmpStatus().getEmpStatus();
			if(empStatus.equals("on_role")||empStatus.equals("on_leave")){
				EmployeeDAO.getInstance().updateEmpLeaves(e1.getEmpId(), "casual", toBeAddedThisQuarter, false);
			}
		}
		
	}
	

}
