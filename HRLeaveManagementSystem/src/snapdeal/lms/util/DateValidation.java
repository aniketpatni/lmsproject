package snapdeal.lms.util;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import snapdeal.lms.entityDAO.*;
public class DateValidation {

	public static boolean LeaveCheck(HttpServletRequest request) {

		int index = 0;
		int empId = ((Employee) request.getSession().getAttribute("emp"))
				.getEmpId();
		Date startDate = Date.valueOf(request.getParameter("leave_from"));
		Date endDate = Date.valueOf(request.getParameter("leave_to"));

		List<LeaveRequest> history = LeaveRequestDAO.getInstance()
				.getLeaveRequestByEmpIdByOrder(empId);
		int size = history.size();
		if (size == 0)
			return true;
		else {
			LeaveRequest myLeave = history.get(index);
			if (startDate.after(myLeave.getLeaveTo()))
				return true;
			else {

				do {
					if (!(endDate.before(myLeave.getLeaveFrom())))
						return false;
					index++;
					if (size > index)
						myLeave = history.get(index);
					else
						return true;
				} while (!(startDate.after(myLeave.getLeaveTo())));
			}

			return true;
		}
	}
}
