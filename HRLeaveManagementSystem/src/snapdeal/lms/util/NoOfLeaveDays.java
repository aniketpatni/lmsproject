package snapdeal.lms.util;

import java.sql.Date;
import java.util.Calendar;
import snapdeal.lms.entityDAO.*;


public class NoOfLeaveDays {
	public static int getNoOfLeaveDays(Date startDate, Date endDate) {
		OfficialHolidaysDAO officialHolidaysDAO = new OfficialHolidaysDAO();
		int noOfOfficialHolidays = officialHolidaysDAO
				.listNoOfHolidaysBetweenDates(startDate, endDate);

		java.util.Date date1 = (java.util.Date) (startDate);
		java.util.Date date2 = (java.util.Date) (endDate);
		int cnt = 0;
		while (!date1.after(date2)) {
			if (date1.getDay() != 0 && date1.getDay() != 6)
				cnt++;
			Calendar c = Calendar.getInstance();
			c.setTime(date1);
			c.add(Calendar.DATE, 1);
			date1 = c.getTime();

		}
		int noOfLeaveDays = cnt - noOfOfficialHolidays;
		return noOfLeaveDays;
	}

}