package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.DesignationLeaveMap;
import snapdeal.lms.entityDAO.DesignationLeaveMapDAO;

/**
 * Servlet implementation class ShowNoOfLeaves
 */
public class ShowNoOfLeaves extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowNoOfLeaves() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String designation = request.getParameter("designation");
		try{
			DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
			DesignationLeaveMap dlm1 = dlmDao.getObjectByDesignation(designation);
			request.setAttribute("desigLeaveMap", dlm1);
			request.getRequestDispatcher("./ShowLeaveByDesignation.jsp").forward(request, response);
		} catch (Exception e){
			request.getRequestDispatcher("./leavemanagement.html").forward(request, response);
		} 
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String designation = request.getParameter("designation");
		try{
			DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
			DesignationLeaveMap dlm1 = dlmDao.getObjectByDesignation(designation);
			request.setAttribute("desigLeaveMap", dlm1);
			request.getRequestDispatcher("./ShowLeaveByDesignation.jsp").forward(request, response);
		} catch (Exception e){
			request.getRequestDispatcher("./leavemanagement.html").forward(request, response);
		} 
		
	}

}
