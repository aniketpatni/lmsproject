package snapdeal.lms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.OfficialHolidaysDAO;

/**
 * Servlet implementation class DeleteHolidayByDate
 */
public class DeleteHolidayByDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteHolidayByDate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String date = request.getParameter("date");
		String holiday = request.getParameter("holiday");
		if (!holiday.equals("")) {
			try {
				OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
				holidayDAO.deleteHolidays(holiday);
			} catch (Exception e) {
				System.out.println("Sorry could not complete transaction !");
				try{
					OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
					holidayDAO.deleteHolidaysByDate(date);
				}
				catch (Exception ex){
					System.out.println("Sorry could not complete transaction !");
				}
			} finally {
				request.getRequestDispatcher("./OutputHolidayList").forward(
						request, response);
			}
		} else {
			try {
				OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
				holidayDAO.deleteHolidaysByDate(date);
			} catch (Exception e) {
				System.out.println("Sorry could not complete transaction !");
			} finally {
				request.getRequestDispatcher("./OutputHolidayList").forward(
						request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String date = request.getParameter("date");
		String holiday = request.getParameter("holiday");
		if (!holiday.equals("")) {
			try {
				OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
				holidayDAO.deleteHolidays(holiday);
			} catch (Exception e) {
				System.out.println("Sorry could not complete transaction !");
				try{
					OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
					holidayDAO.deleteHolidaysByDate(date);
				}
				catch (Exception ex){
					System.out.println("Sorry could not complete transaction !");
				}
			} finally {
				request.getRequestDispatcher("./OutputHolidayList").forward(
						request, response);
			}
		} else {
			try {
				OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
				holidayDAO.deleteHolidaysByDate(date);
			} catch (Exception e) {
				System.out.println("Sorry could not complete transaction !");
			} finally {
				request.getRequestDispatcher("./OutputHolidayList").forward(
						request, response);
			}
		}
	}

}
