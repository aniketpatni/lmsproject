package snapdeal.lms.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class LoginFilter implements Filter {

	
	
	/**
	 * Default constructor.
	 */
	public LoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request1 = ((HttpServletRequest) request);
		HttpServletResponse response1 = ((HttpServletResponse) response);
		String requestURI = request1.getRequestURI();
		
		if(requestURI.contains("/employee")||requestURI.contains("/manager")||requestURI.contains("/hr")||requestURI.contains("/admin")){
			if (request1.getSession().getAttribute("emp") != null) {
				chain.doFilter(request, response);

			} else {

				response1.sendRedirect("/LeaveManagementSystem/login.jsp");
			}
		}else{
			chain.doFilter(request, response);
		}
		
		/*// pass the request along the filter chain
		System.out.println(((HttpServletRequest) request).getRequestURI());
		if (.equals(
				"/LMSPrac3/login.jsp")
				|| ((HttpServletRequest) request).getRequestURI().equals(
						"/LMSPrac3/userlogin")
				|| ((HttpServletRequest) request).getRequestURI().equals(
						"/LMSPrac3/email.jsp")) {
			
		} else if (((HttpServletRequest) request).getRequestURI().equals(
				"/LMSPrac3/js/*")
				|| ((HttpServletRequest) request).getRequestURI().equals(
						"/LMSPrac3/images/*")
				|| ((HttpServletRequest) request).getRequestURI().equals(
						"/LMSPrac3/themes/*")
				|| ((HttpServletRequest) request).getRequestURI().equals(
						"/LMSPrac3/style/*")) {
			chain.doFilter(request, response);
		} else {
			
		}*/

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
