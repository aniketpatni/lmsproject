package snapdeal.lms.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.OfficialHolidaysDAO;

/**
 * Servlet implementation class DeleteHoliday
 */
public class DeleteHolidayByHolidayName extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteHolidayByHolidayName() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String holiday = request.getParameter("holiday");
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		holidayDAO.deleteHolidays(holiday);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/HR.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String holiday = request.getParameter("holiday");
		OfficialHolidaysDAO holidayDAO = new OfficialHolidaysDAO();
		holidayDAO.deleteHolidays(holiday);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/HR.jsp");
		dispatcher.forward(request, response);
	}

}
