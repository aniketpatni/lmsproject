package snapdeal.lms.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.EmployeeDAO;

/**
 * Servlet implementation class ChangeEmpStatus
 */
public class ChangeEmpStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeEmpStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int empId = Integer.parseInt(request.getParameter("empId"));
		String empStatus = request.getParameter("empStatus");
		try{
			EmployeeDAO.getInstance().updateEmployeeStatus(empId, empStatus);
			request.getRequestDispatcher("EmpStatusUpdated.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("EmpStatusNotUpdated.jsp").forward(request, response);
		}}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		String empStatus = request.getParameter("empStatus");
		
		try{
			EmployeeDAO.getInstance().updateEmployeeStatus(empId, empStatus);
			request.getRequestDispatcher("/hr/employeetasks/EmpStatusUpdated.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("/hr/employeetasks/EmpStatusNotUpdated.jsp").forward(request, response);
		}
	}

}
