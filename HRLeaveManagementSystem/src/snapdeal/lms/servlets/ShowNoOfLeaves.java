package snapdeal.lms.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.DesignationLeaveMap;
import snapdeal.lms.entityDAO.DesignationLeaveMapDAO;

/**
 * Servlet implementation class ShowNoOfLeaves
 */
public class ShowNoOfLeaves extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowNoOfLeaves() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String designation = request.getParameter("designation");
		try{
			DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
			DesignationLeaveMap dlm1 = dlmDao.getObjectByDesignation(designation);
			request.setAttribute("desigLeaveMap", dlm1);
			request.getRequestDispatcher("./hr/admintasks/ShowLeaveByDesignation.jsp").forward(request, response);
		} catch (Exception e){
			request.getRequestDispatcher("./hr/admintasks/leavemanagement.html").forward(request, response);
		} 
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String designation = request.getParameter("designation");
		try{
			DesignationLeaveMapDAO dlmDao = new DesignationLeaveMapDAO();
			DesignationLeaveMap dlm1 = dlmDao.getObjectByDesignation(designation);
			request.setAttribute("desigLeaveMap", dlm1);
			request.getRequestDispatcher("./hr/admintasks/ShowLeaveByDesignation.jsp").forward(request, response);
		} catch (Exception e){
			request.getRequestDispatcher("./hr/admintasks/leavemanagement.html").forward(request, response);
		} 
		
	}

}
