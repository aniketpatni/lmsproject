package snapdeal.lms.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import snapdeal.lms.entityDAO.OfficialHolidays;
import snapdeal.lms.entityDAO.OfficialHolidaysDAO;

/**
 * Servlet implementation class OutputHolidayList
 */
public class OutputHolidayList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OutputHolidayList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OfficialHolidaysDAO ohDao = new OfficialHolidaysDAO();
		List<OfficialHolidays> ohList = ohDao.listHolidaysAsc();
		
		request.setAttribute("listOfHolidays", ohList);
		request.getRequestDispatcher("./hr/admintasks/holidaymanagement.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OfficialHolidaysDAO ohDao = new OfficialHolidaysDAO();
		List<OfficialHolidays> ohList = ohDao.listHolidaysAsc();
		
		request.setAttribute("listOfHolidays", ohList);
		request.getRequestDispatcher("./hr/admintasks/holidaymanagement.jsp").forward(request, response);
	}

}
