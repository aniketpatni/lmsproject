package snapdeal.lms.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.LeaveStateLog;
import snapdeal.lms.entityDAO.LeaveStateLogDAO;

/**
 * 
 */

public class ParticularLeaveHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ParticularLeaveHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		int leaveId = Integer.parseInt(request.getParameter("leaveId"));
		try{
			LeaveStateLogDAO LeaveStateLogDAOObject=LeaveStateLogDAO.getInstance();
			List<LeaveStateLog> leaveStateLoglist= LeaveStateLogDAOObject.getHistory(leaveId);
			request.setAttribute("ListOfLogs", leaveStateLoglist);
			request.getRequestDispatcher("/hr/employeetasks/ParticularLeaveLoggedDetails.jsp").forward(request, response);
		} catch (HibernateException he){
			System.out.println("Could not get history for leaveId = " + leaveId);
			request.getRequestDispatcher("OverallLeaveHistory?empId="+empId).forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		int leaveId = Integer.parseInt(request.getParameter("leaveId"));
		try{
			LeaveStateLogDAO LeaveStateLogDAOObject=LeaveStateLogDAO.getInstance();
			List<LeaveStateLog> leaveStateLoglist= LeaveStateLogDAOObject.getHistory(leaveId);
			request.setAttribute("ListOfLogs", leaveStateLoglist);
			request.getRequestDispatcher("/hr/employeetasks/ParticularLeaveLoggedDetails.jsp").forward(request, response);
		} catch (HibernateException he){
			System.out.println("Could not get history for leaveId = " + leaveId);
			request.getRequestDispatcher("OverallLeaveHistory?empId="+empId).forward(request, response);
		}
		
	}

}
