package snapdeal.lms.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.Employee;
import snapdeal.lms.entityDAO.EmployeeDAO;

/**
 * Servlet implementation class AccessEmployeeDetails
 */
public class AccessEmployeeDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccessEmployeeDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		
		try{
			Employee employeeEdit = EmployeeDAO.getInstance().getEmployee(empId);
			request.setAttribute("employeeEdit", employeeEdit);
			request.getRequestDispatcher("/hr/employeetasks/EditEmployeeDetails.jsp").forward(request, response);
		} catch(HibernateException he){
			request.getRequestDispatcher("SearchUser?empId="+empId).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		
		try{
			Employee employeeEdit = EmployeeDAO.getInstance().getEmployee(empId);
			request.setAttribute("employeeEdit", employeeEdit);
			request.getRequestDispatcher("/hr/employeetasks/EditEmployeeDetails.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("SearchUser?empId="+empId).forward(request, response);
		}
		
	}

}
