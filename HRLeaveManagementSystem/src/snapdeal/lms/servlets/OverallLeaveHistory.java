package snapdeal.lms.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.LeaveRequest;
import snapdeal.lms.entityDAO.LeaveRequestDAO;
import snapdeal.lms.entityDAO.LeaveStateLog;
import snapdeal.lms.entityDAO.LeaveStateLogDAO;

/**
 * 
 */

public class OverallLeaveHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OverallLeaveHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId=Integer.parseInt(request.getParameter("empId"));
		try{
			LeaveRequestDAO lrDao = LeaveRequestDAO.getInstance();
			List<LeaveRequest> lrList = lrDao.getLeaveRequestByEmpIdByOrder(empId);
			request.setAttribute("ListOfLeaveRequests", lrList);
			request.getRequestDispatcher("/hr/employeetasks/OverallLeaveDetail.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("SearchUser?empId="+empId).forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int empId=Integer.parseInt(request.getParameter("empId"));
		try{
			LeaveRequestDAO lrDao = LeaveRequestDAO.getInstance();
			List<LeaveRequest> lrList = lrDao.getLeaveRequestByEmpIdByOrder(empId);
			request.setAttribute("ListOfLeaveRequests", lrList);
			request.getRequestDispatcher("/hr/employeetasks/OverallLeaveDetail.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("SearchUser?empId="+empId).forward(request, response);
		}
	}

}
