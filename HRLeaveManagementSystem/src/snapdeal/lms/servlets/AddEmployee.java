package snapdeal.lms.servlets;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;

import snapdeal.lms.entityDAO.EmployeeDAO;

/**
 * Servlet implementation class AddEmployee
 */
public class AddEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int empId = Integer.parseInt(request.getParameter("empId"));
		String name = request.getParameter("empName");
		String dept = request.getParameter("empDept");
		String emailId = request.getParameter("empEmail");
		Date dateOfJoining =Date.valueOf(request.getParameter("joiningDate"));
		int managerId = Integer.parseInt(request.getParameter("managerId"));
		String designation = request.getParameter("designation");
		int sickLeaveBalance = Integer.parseInt(request.getParameter("sickLeaves"));
		int casualLeaveBalance = Integer.parseInt(request.getParameter("casualLeaves"));
		String empStatus = request.getParameter("status");
		try{
			EmployeeDAO.getInstance().addEmployee(empId, name, dept, emailId, dateOfJoining, managerId , designation, sickLeaveBalance, casualLeaveBalance, empStatus, 1);
			request.getRequestDispatcher("./EmployeeAddedSuccesfully.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("./EmployeeNotAddedSuccesfully.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		String name = request.getParameter("empName");
		String dept = request.getParameter("empDept");
		String emailId = request.getParameter("empEmail");
		Date dateOfJoining = new Date(new java.util.Date().getTime());
		int managerId = Integer.parseInt(request.getParameter("managerId"));
		String designation = request.getParameter("designation");
		int sickLeaveBalance = Integer.parseInt(request.getParameter("sickLeaves"));
		int casualLeaveBalance = Integer.parseInt(request.getParameter("casualLeaves"));
		String empStatus = request.getParameter("status");
		try{
			EmployeeDAO.getInstance().addEmployee(empId, name, dept, emailId, dateOfJoining, managerId , designation, sickLeaveBalance, casualLeaveBalance, empStatus, 1);
			request.getRequestDispatcher("./EmployeeAddedSuccesfully.jsp").forward(request, response);
		}catch(HibernateException he){
			request.getRequestDispatcher("./EmployeeNotAddedSuccesfully.jsp").forward(request, response);
		}
	}

}
