package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;



public class LeaveTypeDAO {
	private static volatile LeaveTypeDAO leaveTypeDAO = null;

	private LeaveTypeDAO() {
	}

	public static LeaveTypeDAO getInstance() {
		if (leaveTypeDAO == null) {
			synchronized (LeaveTypeDAO.class) {
				if (leaveTypeDAO == null) {
					leaveTypeDAO = new LeaveTypeDAO();
				}
			}
		}
		return leaveTypeDAO;
	}

	public  int addLeaveType(String leaveType, String leaveTypeDescription){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try{
			LeaveType leaveTypeobj = new LeaveType();
			leaveTypeobj.setLeaveType(leaveType);
			leaveTypeobj.setLeaveTypeDescription(leaveTypeDescription);
			session.save(leaveTypeobj);
			transaction.commit();
			return 1;
		}
		catch(HibernateException e){
			transaction.rollback();
			return 0;
		}
		finally{
			session.close();
		}
	}
	
//	public int updateLeaveType(String newleaveType, int newID){
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = session.beginTransaction();
//		try{
//			LeaveType leaveTypeobj = (LeaveType) session.get(LeaveType.class, newID);
//			leaveTypeobj.setId(newID);
//			session.save(leaveTypeobj);
//			transaction.commit();
//			return 1;
//		}
//		catch(HibernateException e){
//			transaction.rollback();
//			return 0;
//		}
//		finally{
//			session.close();
//		}
//	}
	
	@SuppressWarnings("unchecked")
	public  int deleteLeaveType(String leaveType){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try{
			List<LeaveType> leaveTypeobj = session.createQuery("from LeaveType where leaveType='"+leaveType+"'").list();
			session.delete(leaveTypeobj.get(0));
			transaction.commit();
			return 1;
		}
		catch(HibernateException e){
			transaction.rollback();
			return 0;
		}
		finally{
			session.close();
		}
	}
	
	public  List<LeaveType> getLeaveType(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try{
			@SuppressWarnings("unchecked")
			List<LeaveType> leaveType = session.createQuery("from LeaveType").list();
			transaction.commit();
			return leaveType;
		}
		catch(HibernateException e){
			transaction.rollback();
			return null;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public  LeaveType getObjectByLeaveType(String leaveType){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		try{
			List<LeaveType> leaveTypeList = session.createQuery("from LeaveType where leaveType='"+leaveType+"'").list();
			transaction.commit();
			return leaveTypeList.get(0);
		}
		catch(HibernateException e){
			transaction.rollback();
			return null;
		}
		finally{
			session.close();
		}
	}
}