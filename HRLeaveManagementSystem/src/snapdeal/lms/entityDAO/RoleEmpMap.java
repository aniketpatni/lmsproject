package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="role_emp_map")
public class RoleEmpMap implements Serializable{


		/**
		 * 
		 */
		private static final long serialVersionUID = -5906871733760818273L;
		private int id; 
		private Employee empId;
		private RoleMasterList roles;
		
		public RoleEmpMap() {}

		
		@Id
		@GeneratedValue
		@Column(name="id")
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		@ManyToOne
		@JoinColumn(name="emp_id", referencedColumnName="emp_id")
		public Employee getEmpId(){
			return empId;
		}
		
		public void setEmpId(int empId){
			this.empId=EmployeeDAO.getInstance().getEmployee(empId);
		}


		@ManyToOne
		@JoinColumn(name="roles", referencedColumnName="roles")
		public RoleMasterList getRoles(){
			return roles;
		}
		
		public void setRoles(String roles){
			this.roles=RoleMasterListDAO.listObjectByRole(roles);
		}
		

		
		
}

