package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emp_status_list")
public class EmpStatusList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3023630379492524984L;
	private int id;
	private String empStatus;
	private String statusDesc;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "emp_status", unique=true, nullable=false)
	public String getEmpStatus() {
		return empStatus;
	}

	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}

	@Column(name = "status_desc")
	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	@Override
	public String toString() {
		return "EmpStatusList [id=" + id + ", empStatus=" + empStatus
				+ ", statusDesc=" + statusDesc + "]\n";
	}

}
