package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="state_email_map")
public class StateEmailMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7923756784633393682L;
	private int id;
	private DesignationLeaveMap designation;
	private LeaveStateList prevState;
	private LeaveStateList currState;
	private String emailGroup;
	
	public StateEmailMap(int id, String designation, String prev_state, String curr_state, String email_group) {
		super();
		this.id=id;
		this.designation = new DesignationLeaveMapDAO().getObjectByDesignation(designation);
		this.prevState = LeaveStateListDAO.getLeaveStateListByState(prev_state);
		this.currState = LeaveStateListDAO.getLeaveStateListByState(curr_state);
		this.emailGroup = email_group;
	}
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public StateEmailMap() {
		super();
	}
	
	@ManyToOne
	@JoinColumn(name="designation", referencedColumnName="designation")
	public DesignationLeaveMap getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = new DesignationLeaveMapDAO().getObjectByDesignation(designation);
	}
	
	@ManyToOne
	@JoinColumn(name="prev_state", referencedColumnName="state")
	public LeaveStateList getPrevState() {
		return prevState;
	}
	public void setPrevState(String prev_state) {
		this.prevState = LeaveStateListDAO.getLeaveStateListByState(prev_state);
	}
	
	@ManyToOne
	@JoinColumn(name="curr_state", referencedColumnName="state")
	public LeaveStateList getCurrState() {
		return currState;
	}
	public void setCurrState(String curr_state) {
		this.currState = LeaveStateListDAO.getLeaveStateListByState(curr_state);
	}
	
	@Column(name="email_group", nullable=false)
	public String getEmailGroup() {
		return emailGroup;
	}
	public void setEmailGroup(String email_group) {
		this.emailGroup = email_group;
	}

}
