package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="designation_leave_map")
public class DesignationLeaveMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8646558297095675483L;
	private int id;
	private String designation;
	private int casualLeaveAllowed;
	private int sickLeaveAllowed;
	
	public DesignationLeaveMap(String designation) {
		this.designation = designation;
	}
	
	public DesignationLeaveMap() {}

	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="designation", length=50, nullable=false, unique=true)
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	@Column(name="casual_leave_allowed", nullable=false)
	public int getCasualLeaveAllowed() {
		return casualLeaveAllowed;
	}
	public void setCasualLeaveAllowed(int casualLeaveAllowed) {
		this.casualLeaveAllowed = casualLeaveAllowed;
	}
	
	@Column(name="sick_leave_allowed", nullable=false)
	public int getSickLeaveAllowed() {
		return sickLeaveAllowed;
	}
	public void setSickLeaveAllowed(int sickLeaveAllowed) {
		this.sickLeaveAllowed = sickLeaveAllowed;
	}
	
	@Override
	public String toString() {
		return "Desig_Leave_Map [id=" + id + ", designation=" + designation
				+ ", casual_leave_allowed=" + casualLeaveAllowed
				+ ", sick_leave_allowed=" + sickLeaveAllowed + "]";
	}
	
}
