package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DesignationLeaveMapDAO {

	@SuppressWarnings("unchecked")
	public  int getSickLeavesByDesignation(String designation) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			List<DesignationLeaveMap> dlmList = currSession.createQuery("from DesignationLeaveMap where designation='"+designation+"'").list();
			tx.commit();
			return dlmList.get(0).getSickLeaveAllowed();

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public  int getCasualLeavesByDesignation(String designation) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			List<DesignationLeaveMap> dlmList = currSession.createQuery("from DesignationLeaveMap where designation='"+designation+"'").list();
			tx.commit();
			return dlmList.get(0).getCasualLeaveAllowed();

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public  DesignationLeaveMap getObjectByDesignation(String designation) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			List<DesignationLeaveMap> dlmList = currSession.createQuery("from DesignationLeaveMap where designation='"+designation+"'").list();
			tx.commit();
			return dlmList.get(0);

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return null;
	}
	
	public  int addLeavesAndDesignation(String designation, int casualLeaveAllowed, int sickLeaveAllowed) {
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			DesignationLeaveMap dlm = new DesignationLeaveMap();
			dlm.setDesignation(designation);
			dlm.setSickLeaveAllowed(sickLeaveAllowed);
			dlm.setCasualLeaveAllowed(casualLeaveAllowed);
			currSession.save(dlm);
			tx.commit();
			if(tx.wasCommitted()){
				return 1;
			}

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public int setSickLeavesByDesignation(String designation, int sick_leave_allowed) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			String query="from DesignationLeaveMap where designation='"+designation+"'";
			List<DesignationLeaveMap> dlmList = currSession.createQuery(query).list();
			dlmList.get(0).setSickLeaveAllowed(sick_leave_allowed);
			tx.commit();
			return 1;

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	public int setCasualLeavesByDesignation(String designation, int casual_leave_allowed) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			
			tx = currSession.beginTransaction();
			List<DesignationLeaveMap> dlmList = currSession.createQuery("from DesignationLeaveMap where designation='"+designation+"'").list();
			dlmList.get(0).setCasualLeaveAllowed(casual_leave_allowed);
			tx.commit();
			return 1;

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return 0;
	}
	
	
}
