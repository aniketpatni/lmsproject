package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DesignationDLMapDAO {
	
	@SuppressWarnings("unchecked")
	public String getDLByDesignation(String designation) {
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			List<DesignationDLMap> ddmList = currSession.createQuery("from DesignationDLMap where designation.designation='"+designation+"'").list();
			tx.commit();
			return ddmList.get(0).getDistrib_list();

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String setDLByDesignation(String designation, String distrib_list) {
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			List<DesignationDLMap> ddmList = currSession.createQuery("from DesignationDLMap where designation.designation='"+designation+"'").list();
			ddmList.get(0).setDistrib_list(distrib_list);
			tx.commit();
			return distrib_list;

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String addDLAndDesignation(String designation, String distrib_list, int casualLeaveAllowed, int sickLeaveAllowed) throws NullPointerException{
		Session currSession = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {

			tx = currSession.beginTransaction();
			DesignationDLMap ddm1 = new DesignationDLMap();
			List<DesignationLeaveMap> dlmList = currSession.createQuery("from DesignationLeaveMap where designation.designation='"+designation+"'").list();
			ddm1.setDesignation(dlmList.get(0));
			ddm1.setDistrib_list(distrib_list);
			Long saveId = (Long) currSession.save(ddm1);
			tx.commit();
			return saveId.toString();

		} catch (HibernateException he) {
			System.out.println("Transaction failed ! Rolling back");
			System.out.println(he.getMessage());
			he.printStackTrace();
			if(tx!=null){
				tx.rollback();
			}
		} finally {
			currSession.close();
		}
		return null;
	}

}
