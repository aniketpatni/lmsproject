package snapdeal.lms.entityDAO;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;

@Entity
@Table(name = "leave_state_log")
public class LeaveStateLog implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6967196552240460944L;
	private int id;
	private LeaveRequest leave;
	private Employee emp;
	private String leaveMsg;
	private LeaveStateList prevstate;
	private LeaveStateList currstate;
	private Timestamp timeOfChange;
	private Employee loggerEmp;

	/**
	 * 
	 * @return
	 */
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "leave_id",referencedColumnName="leave_id")
	public LeaveRequest getLeave() {
		return leave;
	}

	/**
	 * 
	 * @param leaveId
	 */
	public void setLeave(LeaveRequest leaveId) {
		this.leave = leaveId;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "emp_id",referencedColumnName="emp_id")
	public Employee getEmp() {
		return emp;
	}

	/**
	 * 
	 * @param emp
	 */
	public void setEmp(Employee emp) {
		this.emp = emp;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "leave_msg", nullable = false)
	public String getLeaveMsg() {
		return leaveMsg;
	}

	/**
	 * 
	 * @param leaveMsg
	 */
	public void setLeaveMsg(String leaveMsg) {
		this.leaveMsg = leaveMsg;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "prev_state",referencedColumnName="state")
	public LeaveStateList getPrevState() {
		return prevstate;
	}

	/**
	 * 
	 * @param prevstate
	 */
	public void setPrevState(LeaveStateList prevstate) {
		this.prevstate = prevstate;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "curr_state",referencedColumnName="state")
	public LeaveStateList getCurrState() {
		return currstate;
	}

	/**
	 * 
	 * @param currstate
	 */
	public void setCurrState(LeaveStateList currstate) {
		this.currstate = currstate;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "time_of_change", columnDefinition="TIMESTAMP")
	public Timestamp getTimeOfChange() {
		return timeOfChange;
	}

	/**
	 * 
	 * @param timeOfChange
	 */
	public void setTimeOfChange(Timestamp timeOfChange) {
		this.timeOfChange = timeOfChange;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "logger_emp_id",referencedColumnName="emp_id")
	public Employee getLoggerEmp() {
		return loggerEmp;
	}

	/**
	 * 
	 * @param loggerEmpId
	 */
	public void setLoggerEmp(Employee loggerEmp) {
		this.loggerEmp = loggerEmp;
	}

	/**
	 * 
	 */
	@Override
	public String toString() {
		return "LeaveStateLog [id=" + id + ", leave=" + leave + ", emp=" + emp
				+ ", leaveMsg=" + leaveMsg + ", prevstate=" + prevstate
				+ ", currstate=" + currstate + ", timeOfChange=" + timeOfChange
				+ ", loggerEmp=" + loggerEmp + "]";
	}
}
