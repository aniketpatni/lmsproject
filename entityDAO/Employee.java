package snapdeal.lms.entityDAO;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5778107485313932988L;
	private int id;
	private int empId;
	private String name;
	private String dept;
	private String emailId;
	private Date dateOfJoining;
	private Employee manager;
	private DesignationLeaveMap designation;
	private int sickLeaveBalance;
	private int casualLeaveBalance;
	private EmpStatusList empStatus;
	private Employee handOverToEmp;

	/**
	 * 
	 * @return
	 */
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "emp_id", nullable = false, unique = true)
	public int getEmpId() {
		return empId;
	}

	/**
	 * 
	 * @param empId
	 */
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "dept", nullable = false)
	public String getDept() {
		return dept;
	}

	/**
	 * 
	 * @param dept
	 */
	public void setDept(String dept) {
		this.dept = dept;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "email_id", nullable = false)
	public String getEmailId() {
		return emailId;
	}

	/**
	 * 
	 * @param emailId
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "date_of_joining", nullable = false)
	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	/**
	 * 
	 * @param dateOfJoining
	 */
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "manager_id", referencedColumnName = "emp_id")
	public Employee getManager() {
		return manager;
	}

	/**
	 * 
	 * @param manager
	 */
	public void setManager(Employee manager) {
		this.manager = manager;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "designation",referencedColumnName="designation")
	public DesignationLeaveMap getDesignation() {
		return designation;
	}

	/**
	 * 
	 * @param designation
	 */
	public void setDesignation(DesignationLeaveMap designation) {
		this.designation = designation;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "sick_leave_balance", nullable = false)
	public int getSickLeaveBalance() {
		return sickLeaveBalance;
	}

	/**
	 * 
	 * @param sickLeaveBalance
	 */
	public void setSickLeaveBalance(int sickLeaveBalance) {
		this.sickLeaveBalance = sickLeaveBalance;
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "casual_leave_balance", nullable = false)
	public int getCasualLeaveBalance() {
		return casualLeaveBalance;
	}

	/**
	 * 
	 * @param casualLeaveBalance
	 */
	public void setCasualLeaveBalance(int casualLeaveBalance) {
		this.casualLeaveBalance = casualLeaveBalance;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "emp_status",referencedColumnName="emp_status")
	public EmpStatusList getEmpStatus() {
		return empStatus;
	}

	/**
	 * 
	 * @param empStatus
	 */
	public void setEmpStatus(EmpStatusList empStatus) {
		this.empStatus = empStatus;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "hand_over_to_emp",referencedColumnName="emp_id")
	public Employee getHandOverToEmp() {
		return handOverToEmp;
	}

	/**
	 * 
	 * @param handOverToEmp
	 */
	public void setHandOverToEmp(Employee handOverToEmp) {
		this.handOverToEmp = handOverToEmp;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", empId=" + empId + ", name=" + name
				+ ", dept=" + dept + ", emailId=" + emailId
				+ ", dateOfJoining=" + dateOfJoining + ", manager=" + manager.getEmpId()
				+ ", designation=" + designation.getDesignation() + ", sickLeaveBalance="
				+ sickLeaveBalance + ", casualLeaveBalance="
				+ casualLeaveBalance + ", empStatus=" + empStatus.getEmpStatus()
				+ ", handOverToEmp=" + handOverToEmp.getEmpId() + "]";
	}

}