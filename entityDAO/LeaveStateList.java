package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="leave_state_list")
public class LeaveStateList implements Serializable{
	
		/**
		* 
		*/
		private static final long serialVersionUID = 4644653554053814601L;
		private int id;
		private String state;
		private String statedesc;
		
		public LeaveStateList() {}
		
		public LeaveStateList(String state) {
			this.state = state;
		}

		@Id
		@GeneratedValue
		@Column(name="id")
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}


		@Column(name="state", nullable=false, unique=true)
		public String getState(){
			return state;
		}
		
		public void setState(String state){
			this.state=state;
		}

		@Column(name="state_desc", nullable=false)
		public String getStateDesc(){
			return statedesc;
		}
		
		public void setStateDesc(String statedesc){
			this.statedesc=statedesc;
		}

		@Override
		public String toString() {
			return "LeaveStateList [id=" + id + ", state=" + state
					+ ", statedesc=" + statedesc + "]\n";
		}

}


