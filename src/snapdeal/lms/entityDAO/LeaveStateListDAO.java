package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LeaveStateListDAO {

	public static void saveLeaveStateList(String state,String statedesc)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			LeaveStateList lsl = new LeaveStateList();
			lsl.setState(state);
			lsl.setStateDesc(statedesc);
			session.save(lsl);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static void listLeaveStateList()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<LeaveStateList> lsles = session.createQuery("from LeaveStateList").list();

			for (LeaveStateList lsl : lsles){
				System.out.println(lsl.getState()+lsl.getStateDesc());
			}

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	
	public static LeaveStateList getLeaveStateListByState(String state){
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		LeaveStateList lsles=null;
		try {
			transaction = session.beginTransaction();
			 lsles= (LeaveStateList) session.createQuery("from LeaveStateList where state='"+state+"'").list().get(0);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lsles;
	}
//
//	public void updatePassword(int empId,String password)
//	{
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		try {
//			transaction = session.beginTransaction();
//			Password pass = (Password) session.get(Password.class, empId);
//			pass.setPassword(password);
//			transaction.commit();
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//		} finally {
//			session.close();
//		}
//	}
//
//	public void deletePassword(int empId)
//	{
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		Transaction transaction = null;
//		try {
//			transaction = session.beginTransaction();
//			Password pass = (Password) session.get(Password.class, empId);
//			session.delete(pass);
//			transaction.commit();
//		} catch (HibernateException e) {
//			transaction.rollback();
//			e.printStackTrace();
//		} finally {
//			session.close();
//		}
//	}
}