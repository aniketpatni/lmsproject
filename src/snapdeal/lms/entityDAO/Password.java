package snapdeal.lms.entityDAO;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="passwd_role")
public class Password implements Serializable{


		/**
	 * 
	 */
	private static final long serialVersionUID = -851712754942127548L;
		private int id;
		private Employee empid;
		private String password;
		
		public Password() {}

		@Id
		@GeneratedValue
		@Column(name="id")
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

//		@Column(name="emp_id", nullable=false)
//		@OneToOne(fetch=FetchType.EAGER)
		@OneToOne
		@JoinColumn(name="emp_id", referencedColumnName="emp_id")
		public Employee getEmpid(){
			return empid;
		}
		
		public void setEmpid(int empId){
			this.empid=EmployeeDAO.getInstance().getEmployee(empId);
		}

		
		
		@Column(name="password", nullable=false)
		public String getPassword(){
			return password;
		}
		
		public void setPassword(String password){
			this.password=password;
		}
}
