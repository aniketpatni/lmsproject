package snapdeal.lms.entityDAO;

import java.sql.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmployeeDAO {
	private static volatile EmployeeDAO employeeDAO = null;

	private EmployeeDAO() {
	}

	public static EmployeeDAO getInstance() {
		if (employeeDAO == null) {
			synchronized (EmployeeDAO.class) {
				if (employeeDAO == null) {
					employeeDAO = new EmployeeDAO();
				}
			}
		}
		return employeeDAO;
	}

	/**
	 * 
	 * @param empId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Employee getEmployee(int empId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Employee> employees = null;
		try {
			transaction = session.beginTransaction();
			String query = "from Employee where empId=" + empId;
			employees = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return employees.get(0);
	}

	/**
	 * 
	 * @param empId
	 * @param name
	 * @param dept
	 * @param emailId
	 * @param dateOfJoining
	 * @param managerId
	 * @param designation
	 * @param sickLeaveBalance
	 * @param casualLeaveBalance
	 * @param empStatus
	 * @param handOverToEmp
	 */
	public void addEmployee(int empId, String name, String dept,
			String emailId, Date dateOfJoining, int managerId,
			String designation, int sickLeaveBalance, int casualLeaveBalance,
			String empStatus, int handOverToEmp) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Employee employee = new Employee();
			employee.setEmpId(empId);
			employee.setName(name);
			employee.setDept(dept);
			employee.setEmailId(emailId);
			employee.setDateOfJoining(dateOfJoining);
			employee.setManager(getEmployee(managerId));
			employee.setDesignation(new DesignationLeaveMapDAO()
					.getObjectByDesignation(designation));
			employee.setSickLeaveBalance(sickLeaveBalance);
			employee.setCasualLeaveBalance(casualLeaveBalance);
			employee.setEmpStatus(EmpStatusListDAO
					.getEmpStatusByStatus(empStatus));
			employee.setHandOverToEmp(getEmployee(handOverToEmp));
			session.save(employee);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param empId
	 * @param name
	 * @param dept
	 * @param emailId
	 * @param dateOfJoining
	 * @param managerId
	 * @param designation
	 * @param sickLeaveBalance
	 * @param casualLeaveBalance
	 * @param empStatus
	 * @param handOverToEmp
	 */
	public void updateEmployeeStatus(int empId, String empStatus) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Employee employee = EmployeeDAO.getInstance().getEmployee(empId);
			employee.setEmpStatus(EmpStatusListDAO
					.getEmpStatusByStatus(empStatus));
//			session.save(employee);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param empId
	 * @param name
	 * @param dept
	 * @param emailId
	 * @param dateOfJoining
	 * @param managerId
	 * @param designation
	 * @param sickLeaveBalance
	 * @param casualLeaveBalance
	 * @param empStatus
	 * @param handOverToEmp
	 */
	public void updateEmployee(int empId, String name, String dept,
			String emailId, Date dateOfJoining, int managerId,
			String designation, int sickLeaveBalance, int casualLeaveBalance,
			String empStatus, int handOverToEmp) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			String query = "from Employee where empId=" + empId;
			Employee employee= (Employee) session.createQuery(query).list().get(0);
			employee.setName(name);
			employee.setDept(dept);
			employee.setEmailId(emailId);
			employee.setDateOfJoining(dateOfJoining);
			employee.setManager(getEmployee(managerId));
			employee.setSickLeaveBalance(sickLeaveBalance);
			employee.setCasualLeaveBalance(casualLeaveBalance);
			employee.setHandOverToEmp(getEmployee(handOverToEmp));
//			session.save(employee);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void updateEmpLeaves(int empId, String leaveType, int leaveApplied, boolean decreaseLeaves) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Employee employee = null;
		int decreaseTrue = -1;
		if(decreaseLeaves){
			decreaseTrue = 1;
		}
		try {
			transaction = session.beginTransaction();
			String query = "from Employee where empId=" + empId;
			employee = (Employee) session.createQuery(query).uniqueResult();
			if ("casual".equals(leaveType)) {
				employee.setCasualLeaveBalance(employee.getCasualLeaveBalance()	- decreaseTrue*leaveApplied);
			} else if ("sick".equals(leaveType)) {
				employee.setSickLeaveBalance(employee.getCasualLeaveBalance() - decreaseTrue*leaveApplied);
			}
//			session.save(employee);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}