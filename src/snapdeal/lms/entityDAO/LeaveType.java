package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "leave_type_list")
public class LeaveType implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9063785590802878494L;
	private int id;
	private String leaveType;
	private String leaveTypeDescription;

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "leave_type", unique=true, nullable=false)
	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	@Column(name = "leave_type_desc")
	public String getLeaveTypeDescription() {
		return leaveTypeDescription;
	}

	public void setLeaveTypeDescription(String leaveTypeDescription) {
		this.leaveTypeDescription = leaveTypeDescription;
	}

	@Override
	public String toString() {
		return "LeaveType [id=" + id + ", leaveType=" + leaveType
				+ ", leaveTypeDescription=" + leaveTypeDescription + "]";
	}
	
}