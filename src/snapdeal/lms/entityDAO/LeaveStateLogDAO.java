package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LeaveStateLogDAO {
	private static volatile LeaveStateLogDAO leaveStateLogDAO = null;

	private LeaveStateLogDAO() {
	}

	public static LeaveStateLogDAO getInstance() {
		if (leaveStateLogDAO == null) {
			synchronized (LeaveStateLogDAO.class) {
				if (leaveStateLogDAO == null) {
					leaveStateLogDAO = new LeaveStateLogDAO();
				}
			}
		}
		return leaveStateLogDAO;
	}

	/**
	 * 
	 * @param leaveId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveStateLog> getHistory(int leaveId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveStateLog> history = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveStateLog where leave.leaveId=" + leaveId;
			history = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return history;
	}
	

	/**
	 * 
	 * @param leaveId
	 * @param empId
	 * @param leaveMessage
	 * @param currstate
	 * @param prevstate
	 * @param timestamp
	 * @param loggerEmpId
	 */
	public void addLog(int leaveId, int empId, String leaveMessage, String currState, String prevState, int loggerEmpId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			LeaveStateLog leaveStateLog = new LeaveStateLog();
			leaveStateLog.setLeave(LeaveRequestDAO.getInstance().getLeaveRequestByLeaveId(leaveId).get(0));
			leaveStateLog.setEmp(EmployeeDAO.getInstance().getEmployee(empId));
			leaveStateLog.setLeaveMsg(leaveMessage);
			leaveStateLog.setPrevState(LeaveStateListDAO.getLeaveStateListByState(prevState));
			leaveStateLog.setCurrState(LeaveStateListDAO.getLeaveStateListByState(currState));
			leaveStateLog.setLoggerEmp(EmployeeDAO.getInstance().getEmployee(loggerEmpId));
			session.save(leaveStateLog);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
