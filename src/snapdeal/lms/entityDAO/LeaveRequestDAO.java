package snapdeal.lms.entityDAO;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LeaveRequestDAO {
	private static volatile LeaveRequestDAO leaveRequestDAO = null;

	private LeaveRequestDAO() {
	}

	public static LeaveRequestDAO getInstance() {
		if (leaveRequestDAO == null) {
			synchronized (LeaveRequestDAO.class) {
				if (leaveRequestDAO == null) {
					leaveRequestDAO = new LeaveRequestDAO();
				}
			}
		}
		return leaveRequestDAO;
	}

	/**
	 * 
	 * @param empId
	 * @param leaveFrom
	 * @param leaveTo
	 * @param appliedOn
	 * @param state
	 * @param typeOfLeave
	 * @param leaveReason
	 * @param noOfLeaveDays
	 * @return
	 */
	public int addLeaveRequest(int empId, Date leaveFrom, Date leaveTo,
			String state, String typeOfLeave,
			String leaveReason, int noOfLeaveDays) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Transaction transaction2 = null;
		//Transaction transaction3 = null;
		int leaveId = 0;
		try {
			transaction = session.beginTransaction();
			LeaveRequest leaveRequest = new LeaveRequest();
			leaveRequest.setEmp(EmployeeDAO.getInstance().getEmployee(empId));
			leaveRequest.setLeaveFrom(leaveFrom);
			leaveRequest.setLeaveTo(leaveTo);
			leaveRequest.setState(LeaveStateListDAO
					.getLeaveStateListByState(state));
			leaveRequest.setLeaveType(LeaveTypeDAO.getInstance().getObjectByLeaveType(typeOfLeave));
			leaveRequest.setLeaveReason(leaveReason);
			leaveId = (Integer) session.save(leaveRequest);
			transaction.commit();
			transaction2 = session.beginTransaction();
			LeaveStateLogDAO.getInstance().addLog(leaveId, empId, leaveReason,
					"pending_approval_by_manager", "NULL", empId);
			transaction2.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			if (transaction2 != null) {
				LeaveRequestDAO.getInstance().deleteLeaveRequest(leaveId);
				transaction2.rollback();
			}
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveId;
	}

	/**
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequests() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest";
			leaveRequests = session.createQuery(query).list();
			/*
			 * for (LeaveRequest leaveRequest : leaveRequests) {
			 * System.out.println(leaveRequest.toString()); }
			 */

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

	/**
	 * 
	 * @param leaveId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequestByLeaveId(int leaveId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest where leaveId=" + leaveId;
			leaveRequests = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

	/**
	 * 
	 * @param leaveId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequestByEmpIdByOrder(int empId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest where emp.empId=" + empId
					+ " order by leaveTo desc";
			leaveRequests = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

	/**
	 * 
	 * @param leaveId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequestByManagerId(int managerId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest where emp in (from Employee where manager.empId="+managerId+")";
			leaveRequests = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

	/**
	 * 
	 * @param empId
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequestsByEmpId(int empId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest where emp_id=" + empId;
			leaveRequests = session.createQuery(query).list();

			for (LeaveRequest leaveRequest : leaveRequests) {
				System.out.println(leaveRequest.toString());
			}

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

	/**
	 * 
	 * @param leaveId
	 * @param empId
	 * @param leaveFrom
	 * @param leaveTo
	 * @param appliedOn
	 * @param state
	 * @param leaveType
	 * @param leaveReason
	 * @param noOfLeaveDays
	 */
	public void updateLeaveRequest(int leaveId, int empId, Date leaveFrom,
			Date leaveTo, Timestamp appliedOn, String state, String leaveType,
			String leaveReason, int noOfLeaveDays) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			LeaveRequest leaveRequest = (LeaveRequest) session.get(
					LeaveRequest.class, leaveId);
			leaveRequest.setEmp(EmployeeDAO.getInstance().getEmployee(empId));
			leaveRequest.setLeaveFrom(leaveFrom);
			leaveRequest.setLeaveTo(leaveTo);
			leaveRequest.setAppliedOn(appliedOn);
			leaveRequest.setState(LeaveStateListDAO
					.getLeaveStateListByState(state));
			leaveRequest.setLeaveType(LeaveTypeDAO.getInstance()
					.getObjectByLeaveType(leaveType));
			leaveRequest.setLeaveReason(leaveReason);

			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void updateLeaveRequestState(int leaveId, String state,	String leaveMessage, int loggerEmpId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			LeaveRequest leaveRequest = (LeaveRequest) session.get(LeaveRequest.class, leaveId);
			String prevState = leaveRequest.getState().getState();
			int empId = leaveRequest.getEmp().getEmpId();
			leaveRequest.setState(LeaveStateListDAO.getLeaveStateListByState(state));
			LeaveStateLogDAO.getInstance().addLog(leaveId, empId, leaveMessage,	state, prevState, loggerEmpId);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 *reduntant function not used in the system 
	 * @param leaveId
	 */
	public void deleteLeaveRequest(int leaveId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			LeaveRequest leaveRequest = (LeaveRequest) session.get(
					LeaveRequest.class, leaveId);
			session.delete(leaveRequest);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param empId
	 */
	@SuppressWarnings("unchecked")
	public List<LeaveRequest> getLeaveRequestByState(int empId, String state) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<LeaveRequest> leaveRequests = null;
		String diffState = null;
		if (state.equals("active")) {
			diffState = "like 'pending_approval%'";
		} else {
			diffState = "not like 'pending_approval%'";
		}
		try {
			transaction = session.beginTransaction();
			String query = "from LeaveRequest where emp.empId=" + empId
					+ " and state " + diffState;
			leaveRequests = session.createQuery(query).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return leaveRequests;
	}

}