package snapdeal.lms.entityDAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="designation_dl_map")
public class DesignationDLMap implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7415544846789386921L;
	private int id;
	private DesignationLeaveMap designation;
	private String distrib_list;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="designation", referencedColumnName="designation")
	public DesignationLeaveMap getDesignation() {
		return designation;
	}
	public void setDesignation(DesignationLeaveMap designation) {
		this.designation = designation;
	}
	
	@Column(name="distribution_list", columnDefinition="TEXT", nullable=false)
	public String getDistrib_list() {
		return distrib_list;
	}
	public void setDistrib_list(String distrib_list) {
		this.distrib_list = distrib_list;
	}
	@Override
	public String toString() {
		return "DesignationDLMap [id=" + id + ", designation=" + designation
				+ ", distrib_list=" + distrib_list + "]\n";
	}

	
}
