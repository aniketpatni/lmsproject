package snapdeal.lms.entityDAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * 
 * @author arif
 * 
 */
public class RoleMasterListDAO {

	public static int addRoleMasterList(String roles, String roledesc) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		

		try {
			transaction = session.beginTransaction();
			RoleMasterList roleMasterList = new RoleMasterList();

			roleMasterList.setRoles(roles);
			roleMasterList.setRoleDesc(roledesc);
			 session.save(roleMasterList);
			transaction.commit();
			return 1;
		} catch (HibernateException e) {
			transaction.rollback();
			return 0;
		
		} finally {
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	public static List<RoleMasterList> listRoleMasterList() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<RoleMasterList> roleMasterLists = session.createQuery(
					"from RoleMasterList").list();

			/*for (RoleMasterList roleMasterList : roleMasterLists) {
				System.out.println(roleMasterList.toString());
			}*/

			transaction.commit();
			return roleMasterLists;
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static RoleMasterList listObjectByRole(String roles) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			List<RoleMasterList> roleMasterLists = session.createQuery("from RoleMasterList where roles='"+roles+"'").list();
			transaction.commit();
			return roleMasterLists.get(0);
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public static void updateRoleMasterList(String roles, String roledesc) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			RoleMasterList roleMasterList = (RoleMasterList) session.get(
					RoleMasterList.class, roles);
			
			roleMasterList.setRoles(roles);
			roleMasterList.setRoleDesc(roledesc);
			session.save(roleMasterList);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			
		} finally {
			session.close();
		}
	}

	public static void deleteRoleMasterList(String roles) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			RoleMasterList roleMasterList = (RoleMasterList) session.get(
					RoleMasterList.class, roles);
			session.delete(roleMasterList);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
