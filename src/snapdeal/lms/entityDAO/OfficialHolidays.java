package snapdeal.lms.entityDAO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "official_holidays")
public class OfficialHolidays implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4270332372924943628L;
	private long id;
	private String holiday;
	private Date date;

	public OfficialHolidays(String name, Date date) {
		super();
		this.holiday = name;
		this.date = date;
	}

	public OfficialHolidays() {
	}

	@Id
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "holiday", nullable = false)
	public String getHoliday() {
		return holiday;
	}

	public void setHoliday(String name) {
		this.holiday = name;
	}
	
	@Column(name = "date", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
